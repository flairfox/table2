package atom.components.Table2;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.OverlayLayout;
import javax.swing.SwingConstants;
import javax.swing.event.MouseInputAdapter;
import net.miginfocom.swing.MigLayout;

/**
 * Заголовок таблицы
 *
 * @author Добровольский
 */
public class Widget_TableHeader extends JComponent
{

    protected Widget_TableWrap _OwnerTable;
    private ColumnHeaders _ColumnHeaders;
    private ColumnWidthControllers _ColumnWidthControllers;

    public Widget_TableHeader(Widget_TableWrap ownerTable)
    {
        _OwnerTable = ownerTable;
        _ColumnHeaders = new ColumnHeaders();
        _ColumnWidthControllers = new ColumnWidthControllers();

        setLayout(new OverlayLayout(this));

        for (Column column : _OwnerTable.getColumns())
        {
            _ColumnHeaders.addColumnHeader(column);
            _ColumnWidthControllers.addColumnWidthControl(column);
        }

        add(_ColumnHeaders, 0);
        add(_ColumnWidthControllers, 0);

    }

    public void updateWidth()
    {
        for (ColumnHeader columnHeader : _ColumnHeaders._ColumnHeaders)
        {
            columnHeader.updateWidth();
        }

        for (ColumnWidthControllers.ColumnWidthController columnWidthController : _ColumnWidthControllers._ColumnWidthControllers)
        {
            columnWidthController.updateWidth();
        }
    }

    public void clear()
    {
        removeAll();

        _ColumnHeaders = new ColumnHeaders();
        _ColumnWidthControllers = new ColumnWidthControllers();

        add(_ColumnHeaders, 0);
        add(_ColumnWidthControllers, 0);
    }

    public void addColumnHeader(Column column)
    {
        _ColumnHeaders.addColumnHeader(column);
        _ColumnWidthControllers.addColumnWidthControl(column);
    }

    public class ColumnWidthControllers extends JComponent
    {

        private LinkedList<ColumnWidthController> _ColumnWidthControllers;
        private Listener_ColumnWidthControllers _WidthControllersListener;

        public ColumnWidthControllers()
        {
            setLayout(new MigLayout("flowx, fill", "0[pref]0", "0[pref]0"));

            _ColumnWidthControllers = new LinkedList<ColumnWidthController>();
            _WidthControllersListener = new Listener_ColumnWidthControllers(this, _OwnerTable);
            add(Box.createRigidArea(new Dimension(33, 0)));
        }

        public void addColumnWidthControl(Column column)
        {
            ColumnWidthController columnWidthController = new ColumnWidthController(column);
            columnWidthController.addListener(_WidthControllersListener);
            _ColumnWidthControllers.add(columnWidthController);
            add(columnWidthController, "growy");
        }

        public class ColumnWidthController extends JComponent
        {

            protected Column _Column;
            private ColumnWidthSpacer _ColumnWidthSpacer;
            private ColumnWidthManipulationGrip _ColumnSizeManipulationGrip;

            public ColumnWidthController(Column column)
            {
                _Column = column;
                setLayout(new MigLayout("flowx, filly", "0[pref]0", "0[pref]0"));

                _ColumnWidthSpacer = new ColumnWidthSpacer();
                _ColumnWidthSpacer.setWidth(_Column.getWidth());

                _ColumnSizeManipulationGrip = new ColumnWidthManipulationGrip();

                add(_ColumnWidthSpacer);
                add(_ColumnSizeManipulationGrip, "growy");
            }

            public void updateWidth()
            {
                _ColumnWidthSpacer.setWidth(_Column.getWidth());
                revalidate();
            }

            public void setWidth(int width)
            {
                _ColumnWidthSpacer.setWidth(width);
                revalidate();
            }

            public int getIndex()
            {
                return _ColumnWidthControllers.indexOf(this);
            }

            public void addListener(MouseInputAdapter l)
            {
                _ColumnSizeManipulationGrip.addListener(l);
            }

            private class ColumnWidthSpacer extends JComponent
            {

                public void setWidth(int width)
                {
                    setPreferredSize(new Dimension(width - 4, 0));
                    revalidate();
                }

            }

            private class ColumnWidthManipulationGrip extends JComponent
            {

                public ColumnWidthManipulationGrip()
                {
                    setPreferredSize(new Dimension(5, 30));
                    setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                }

                public void addListener(MouseInputAdapter l)
                {
                    addMouseListener(l);
                    addMouseMotionListener(l);
                }

//                @Override
//                protected void paintComponent(Graphics g)
//                {
//                    g.setColor(Color.RED);
//                    g.fillRect(0, 0, getWidth(), getHeight());
//                }
            }
        }

    }

    /**
     * Набор из заголовков столбцов
     */
    public class ColumnHeaders extends JComponent
    {

        private LinkedList<ColumnHeader> _ColumnHeaders;

        public ColumnHeaders()
        {
            setLayout(new MigLayout("flowx", "0[]1[]0", "0[]0[]0"));

            _ColumnHeaders = new LinkedList<ColumnHeader>();

            add(new TopLeftCornerCell(), "west, gapright 1px");
        }

        public void addColumnHeader(Column column)
        {
            ColumnHeader columnHeader = new ColumnHeader(column);
            _ColumnHeaders.add(columnHeader);
            add(columnHeader, "growy");
        }

    }

    /**
     * Заголовок столбца
     */
    public class ColumnHeader extends JLabel
    {

        Column _Column;

        public ColumnHeader(Column column)
        {
            _Column = column;

            setBorder(BorderFactory.createEmptyBorder());
            setBackground(Color.LIGHT_GRAY);
            Font font = getFont();
            setFont(font.deriveFont(Font.BOLD));

            setHorizontalAlignment(SwingConstants.CENTER);

            setPreferredSize(new Dimension(150, 30));

            setText(_Column.getCaption());
        }

        @Override
        protected void paintComponent(Graphics g)
        {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(new Color(180, 217, 241));
            g2.fillRect(0, 0, getWidth(), getHeight());

            super.paintComponent(g);
        }

        public void updateWidth()
        {
            setPreferredSize(new Dimension(_Column.getWidth(), 30));
            revalidate();
            repaint();
        }

        public void setWidth(int width)
        {
            setPreferredSize(new Dimension(width, 30));
            revalidate();
            repaint();
        }
    }

    /**
     * Заглушка
     */
    public class TopLeftCornerCell extends JComponent
    {

        public TopLeftCornerCell()
        {
            setPreferredSize(new Dimension(30, 30));
            setBorder(BorderFactory.createEmptyBorder());
        }

        @Override
        protected void paintComponent(Graphics g)
        {
            g.setColor(new Color(180, 217, 241));
            g.fillRect(0, 0, getWidth(), getHeight());
        }

    }

    public class Listener_ColumnWidthControllers extends MouseInputAdapter
    {

        int _xPressed, _PrevWidth, _NewWidth;
        ColumnWidthControllers _WidthControllers;
        ColumnWidthControllers.ColumnWidthController _WidthController;
        Widget_TableWrap _OwnerTable;
        Container _Parent;

        public Listener_ColumnWidthControllers(ColumnWidthControllers widthControls, Widget_TableWrap ownerTable)
        {
            _WidthControllers = widthControls;
            _OwnerTable = ownerTable;
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            _xPressed = e.getXOnScreen();

            _WidthController = ((ColumnWidthControllers.ColumnWidthController) ((ColumnWidthControllers.ColumnWidthController.ColumnWidthManipulationGrip) e.getSource()).getParent());
            _PrevWidth = _OwnerTable.getColumn(_WidthController.getIndex()).getWidth();

            _Parent = (Container) _WidthControllers;
            while (!(_Parent instanceof JFrame))
            {
                _Parent = _Parent.getParent();
            }

            _Parent.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            _Parent.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }

        @Override
        public void mouseDragged(MouseEvent e)
        {
            _NewWidth = _PrevWidth + (e.getXOnScreen() - _xPressed);

            if (_NewWidth >= _OwnerTable.getColumn(_WidthController.getIndex()).getMinimumWidth())
            {
                _WidthController.setWidth(_NewWidth);
                _ColumnHeaders._ColumnHeaders.get(_WidthController.getIndex()).setWidth(_NewWidth);

                _OwnerTable.getColumn(_WidthController.getIndex()).setWidth(_NewWidth);
            }
        }
    }
}
