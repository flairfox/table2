package atom.components.Table2;

import atom.data.TableModel2.ITable;

/**
 * Интерфейс для подключения рендереров табличных строк
 *
 * @author Добровольский
 */
public interface IRenderer_RowTable extends IRenderer_RowWidget
{
    @Override
    public Widget_RowWidget_Cells createRowWidget(ITable.IRow modelRow);
}