package atom.components.Table2;

/**
 *
 * @author Добровольский
 */
public class Widget_RowHeaderContainer extends ATOMWidget_ExpandableStructure
{

    public void setSelected(boolean flag)
    {
        Widget_RowHeader_View rowHeaderView = (Widget_RowHeader_View) _MainWidget;
        rowHeaderView.setSelected(flag);

        revalidate();
    }
    
    public void setExpandable(boolean flag)
    {
        Widget_RowHeader_View rowHeaderView = (Widget_RowHeader_View) _MainWidget;
        rowHeaderView.setExpandable(flag);
    }
}
