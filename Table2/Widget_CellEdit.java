package atom.components.Table2;

import java.awt.event.KeyListener;
import javax.swing.JComponent;
import net.miginfocom.swing.MigLayout;

/**
 * Виджет редактирования ячейки
 *
 * @author Добровольский
 */
public abstract class Widget_CellEdit extends JComponent
{

    public Widget_CellEdit()
    {
        setLayout(new MigLayout("fill", "0[pref]0", "0[pref]0"));
    }

    public abstract void setInitValue(Object value);

    public abstract Object getNewValue();

    @Override
    public abstract void requestFocus();

    @Override
    public abstract void addKeyListener(KeyListener l);
}
