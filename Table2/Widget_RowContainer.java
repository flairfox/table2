package atom.components.Table2;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.border.Border;

/**
 * Контейнер пользовательского виджета отображения строки (RowWidget). Встроена
 * возможность "раскрывающегося" ряда, позволяющая отображать вложенный в строку
 * пользовательский виджет (NestedWidget).
 *
 * @author Добровольский
 */
public class Widget_RowContainer extends ATOMWidget_ExpandableStructure
{

    public void setSelected(boolean flag)
    {
        ((Widget_RowWidget_Cells) _MainWidget).setSelected(flag);
    }

    public void setBackgroundColor(Color color)
    {
        ((Widget_RowWidget_Cells) _MainWidget).setBackgroundColor(color);
    }
    
    public void setTextColor(Color color)
    {
        ((Widget_RowWidget_Cells) _MainWidget).setTextColor(color);
    }
}
