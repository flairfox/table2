package atom.components.Table2;

import atom.data.TableModel2.ITable;

/**
 * Рендерер базовой табличной строки, состоящего из ячеек
 *
 * @author Добровольский
 */
public class Renderer_RowCells implements IRenderer_RowTable
{

    @Override
    public Widget_RowWidget_Cells createRowWidget(ITable.IRow modelRow)
    {
        return new Widget_RowWidget_Cells();
    }
}
