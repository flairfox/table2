package atom.components.Table2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * Виджет для редактирования текстовой информации
 *
 * @author Добровольский
 */
public class Widget_CellEdit_Text extends Widget_CellEdit
{

    private JTextField _TextField;

    public Widget_CellEdit_Text()
    {
        _TextField = new JTextField();
        _TextField.setBackground(Color.WHITE);

        Border line = BorderFactory.createLineBorder(Color.BLACK);
        Border empty = new EmptyBorder(0, 1, 0, 1);
        CompoundBorder border = new CompoundBorder(line, empty);
        _TextField.setBorder(border);

        _TextField.setMinimumSize(new Dimension(150, 30));

        add(_TextField, "grow");
    }

    @Override
    public void setInitValue(Object value)
    {
        if (value != null)
        {
            _TextField.setText(value.toString());
        } else
        {
            _TextField.setText("");
        }
    }

    @Override
    public Object getNewValue()
    {
        return _TextField.getText();
    }

    @Override
    public void addKeyListener(KeyListener l)
    {
        _TextField.addKeyListener(l);
    }

    @Override
    public void requestFocus()
    {
        _TextField.requestFocus();
        _TextField.selectAll();
    }
    
    @Override
    protected void paintComponent(Graphics g)
    {
        g.setColor(Color.cyan);
        g.fillRect(0, 0, getWidth(), getHeight());
    }
}
