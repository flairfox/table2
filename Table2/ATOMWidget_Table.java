/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atom.components.Table2;

import atom.data.TableModel2.ITable;
import atom.data.TableModel2.ITableView;
import atom.events2.ATOMEventListenerAdapter;
import atom.events2.IATOMEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Добровольский
 */
public class ATOMWidget_Table extends JComponent
{

    private Widget_TableWrap _TableWrap;
    private JScrollPane _ScrollPane;
    private Widget_ToolBar _Toolbar;

    private Widget_CellContainer _SelectedCell;
    private Row _SelectedRow;

    private JButton _BackgroundColorButton;
    private JButton _TextColorButton;
    private JButton _TextFontButton;

    public ATOMWidget_Table()
    {
        setLayout(new MigLayout("fill", "0[pref]0", "0[pref]0"));

        _Toolbar = new Widget_ToolBar();
        _TableWrap = new Widget_TableWrap();
        _ScrollPane = new JScrollPane(_TableWrap);

        //add(_Toolbar, "north");
        add(_ScrollPane, "grow");

        _TableWrap.Delegate().AddListener(_CommonListener);

        _BackgroundColorButton = new JButton("Background Color");
        _BackgroundColorButton.addActionListener(new Listener_BackgroundColorButton());
        _BackgroundColorButton.setEnabled(false);
        _Toolbar.addTool(_BackgroundColorButton);

        _TextColorButton = new JButton("Text Color");
        _TextColorButton.addActionListener(new Listener_TextColorButton());
        _TextColorButton.setEnabled(false);
        _Toolbar.addTool(_TextColorButton);

        _TextFontButton = new JButton("Text Font");
        _TextFontButton.addActionListener(new Listener_TextFontButton());
        _TextFontButton.setEnabled(false);
        _Toolbar.addTool(_TextFontButton);
    }

    public void setModel(ITable tableModel)
    {
        _TableWrap.setModel(tableModel);
    }

    public void setView(ITableView tableView)
    {
        _TableWrap.setModelView(tableView);
    }

    private boolean OnDo_CellSelected(Widget_TableWrap.Events.CellSelected event)
    {
        _SelectedCell = event.SelectedCell;
        _SelectedRow = null;

        if (_SelectedCell != null)
        {
            _BackgroundColorButton.setEnabled(true);
            _TextColorButton.setEnabled(true);
            _TextFontButton.setEnabled(true);
        } else
        {
            _BackgroundColorButton.setEnabled(false);
            _TextColorButton.setEnabled(false);
            _TextFontButton.setEnabled(false);
        }

        return true;
    }

    private boolean OnDo_RowSelected(Widget_RowStructure.Events.RowSelected event)
    {
        _SelectedRow = event.SelectedRow;
        _SelectedCell = null;

        if (_SelectedRow != null)
        {
            _BackgroundColorButton.setEnabled(true);
            _TextColorButton.setEnabled(true);
            _TextFontButton.setEnabled(true);
        } else
        {
            _BackgroundColorButton.setEnabled(false);
            _TextColorButton.setEnabled(false);
            _TextFontButton.setEnabled(false);
        }

        return true;
    }

    private class Listener_BackgroundColorButton implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            {
                Color color;

                if (_SelectedCell != null)
                {
                    color = JColorChooser.showDialog(_Toolbar, "Choose your destiny!", _SelectedCell.getBackgroundColor());

                    if (color != null)
                    {
                        _SelectedCell.setBackgroundColor(color);
                    }
                }

                if (_SelectedRow != null)
                {
                    color = JColorChooser.showDialog(_Toolbar, "Flawless victory!", _SelectedRow.getBackgroundColor());

                    if (color != null)
                    {
                        _SelectedRow.setBackgroundColor(color);
                    }
                }
            }
        }
    }

    private class Listener_TextColorButton implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            {
                Color color;

                if (_SelectedCell != null)
                {
                    color = JColorChooser.showDialog(_Toolbar, "Choose your destiny!", _SelectedCell.getTextColor());

                    if (color != null)
                    {
                        _SelectedCell.setTextColor(color);
                    }
                }

                if (_SelectedRow != null)
                {
                    color = JColorChooser.showDialog(_Toolbar, "Choose your destiny!", _SelectedRow.getTextColor());

                    if (color != null)
                    {
                        _SelectedRow.setTextColor(color);
                    }
                }
            }
        }
    }

    private class Listener_TextFontButton implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            Font font;
            ATOMWidget_FontChooser fontChooser = new ATOMWidget_FontChooser();

            if (_SelectedCell != null)
            {
                fontChooser.setSelectedFont(_SelectedCell.getTextFont());
                if (fontChooser.showDialog(_Toolbar) == ATOMWidget_FontChooser.OK_OPTION)
                {
                    font = fontChooser.getSelectedFont();
                    _SelectedCell.setTextFont(font);
                }
            }
        }
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="##### УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ #####">
    protected CommonListener _CommonListener = new CommonListener();

    protected class CommonListener extends ATOMEventListenerAdapter
    {

        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.Event1
            //if (TEventSource.Events.Event1.class.isAssignableFrom(eventClass))
            //{
            //  TEventSource.Events.Event1 event = (TEventSource.Events.Event1) e;
            //  isAllowEvent = OnBefore_Event1(event);
            //}
            return isAllowEvent;
        }

        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.CellSelected
            if (Widget_TableWrap.Events.CellSelected.class.isAssignableFrom(eventClass))
            {
                Widget_TableWrap.Events.CellSelected event = (Widget_TableWrap.Events.CellSelected) e;
                isDone = OnDo_CellSelected(event);
            }

            //2. Обработка события TEventSource.Events.RowSelected
            if (Widget_RowStructure.Events.RowSelected.class.isAssignableFrom(eventClass))
            {
                Widget_RowStructure.Events.RowSelected event = (Widget_RowStructure.Events.RowSelected) e;
                isDone = OnDo_RowSelected(event);
            }

            return isDone;
        }

        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            //1. Обработка события TEventSource.Events.Event1
            Class<?> eventClass = e.getClass();

            //if (TEventSource.Events.Event1.class.isAssignableFrom(eventClass))
            //{
            //  TEventSource.Events.Event1 event = (TEventSource.Events.Event1) e;
            //  isDone  = OnAfter_Event1(event);
            // }
        }
    }
//</editor-fold> КОНЕЦ "УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ"
//##################################################################

}
