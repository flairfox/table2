package atom.components.Table2;

import atom.data.TableModel2.ITable;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Добровольский
 */
public class Widget_CellContainer extends JComponent
{

    private Row _OwnerRow;

    private ITable.ICell _ModelCell;

    private Object _Value;

    private Widget_CellView _ViewWidget;

    private boolean _isSelected;

    public Widget_CellContainer(Row ownerRow)
    {
        _OwnerRow = ownerRow;
        _isSelected = false;

        setLayout(new MigLayout("fill", "0[pref]0", "0[pref]0"));
        setPreferredSize(new Dimension(150, 30));
    }

    public void setModelCell(ITable.ICell modelCell)
    {
        _ModelCell = modelCell;
        update();
    }

    public void setViewWidget(Widget_CellView viewWidget)
    {
        _ViewWidget = viewWidget;
        add(_ViewWidget, "grow");
    }

    public Widget_CellView getViewWidget()
    {
        return _ViewWidget;
    }

    public void setWidth(int width)
    {
        setPreferredSize(new Dimension(width, 30));
        revalidate();
    }

    public void setBackgroundColor(Color color)
    {
        _ViewWidget.setBackgroundColor(color);
    }

    public Color getBackgroundColor()
    {
        return _ViewWidget.getBackgroundColor();
    }

    public void setTextColor(Color color)
    {
        _ViewWidget.setTextColor(color);
    }

    public Color getTextColor()
    {
        return _ViewWidget.getTextColor();
    }
    
    public void setTextFont(Font font)
    {
        _ViewWidget.setTextFont(font);
    }

    public Font getTextFont()
    {
        return _ViewWidget.getTextFont();
    }

    public void setValue(Object value)
    {
        _Value = value;

        if (_ModelCell != null)
        {
            _ModelCell.setValue(_Value);
        }

        _ViewWidget.updateValue(_Value);
    }

    public Object getValue()
    {
        return _Value;
    }

    public int getColumnIndex()
    {
        return ((Widget_RowWidget_Cells) _OwnerRow.getRowWidget()).getCells().indexOf(this);
    }

    public int getMinimumWidth()
    {
        return _ViewWidget.getMinimumWidth();
    }

    public void update()
    {
        if (_ModelCell != null)
        {
            _Value = _ModelCell.getValue();
            _ViewWidget.updateValue(_Value);
        }
    }

    @Override
    public void paintChildren(Graphics g)
    {
        super.paintChildren(g);

        if (_isSelected)
        {
            Graphics2D g2 = (Graphics2D) g;

            g2.setColor(new Color(30, 119, 162));
            g2.setStroke(new BasicStroke(4));
            g2.drawRect(0, 0, getWidth(), getHeight());
        }
    }

    public void setSelected(boolean flag)
    {
        _isSelected = flag;
        repaint();
    }

    public Row getOwnerRow()
    {
        return _OwnerRow;
    }
}
