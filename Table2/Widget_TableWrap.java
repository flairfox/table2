package atom.components.Table2;

import atom.data.DataTypeInfo;
import atom.data.TableModel2.ITable;
import atom.data.TableModel2.ITableView;
import atom.data.TableModel2.Abstract_Table;
import atom.data.TableModel2.Model_Table;
import atom.events2.ATOMEventListenerAdapter;
import atom.events2.IATOMEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.OverlayLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import net.miginfocom.swing.MigLayout;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogic;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.ATOMEvent;
import atom.events2.ATOMEventDelegate;
import atom.events2.IATOMEventDelegate;
import atom.names.ATOMName;
import atom.names.IATOMName;

/**
 * Табличная организация данных
 *
 * @author Добровольский
 */
public class Widget_TableWrap extends JComponent implements IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    /**
     * Модель данных
     */
    private ITable _DataModel;

    /**
     * Обертка вокруг модели данных
     */
    private ITableView _DataView;

    /**
     * Строковая организация данных
     */
    private Widget_RowStructure _RowStructure;

    /**
     * Коллекция столбцов таблицы
     */
    protected LinkedList<Column> _Columns;

    /**
     * Коллекция рендереров по умолчанию для ячеек
     */
    private HashMap<DataTypeInfo.ELogicTypes, IRenderer_Cell> _CellRenderers;

    /**
     * Слушатель модели данных
     */
    protected CommonListener _ModelListener;

    protected Layer_View _LayerView;
    private Layer_Edit _LayerEdit;

    private Widget_TableHeader _TableHeader;

    private Widget_CellContainer _SelectedTableCell;
    private Row _SelectedTableRow;

    //private Widget_Tools _Tools;
    /**
     * Инициализация таблицы
     */
    public Widget_TableWrap()
    {
        // Создаем модель данных по умолчанию для таблицы
        //Abstract_Table defaultModel = new Model_Table();
        //defaultModel.initTable();

        // Инициализируем поля
        _RowStructure = new Wrap_RowStructure();
        _Columns = new LinkedList<Column>();
        _CellRenderers = new HashMap<DataTypeInfo.ELogicTypes, IRenderer_Cell>();
        _ModelListener = new CommonListener();
        _TableHeader = new Widget_TableHeader(this);
        //_Tools = new Widget_Tools();

        // Регистрируем рендереры по умолчанию для виджетов ряда
        //registerRowRenderer(Abstract_Row.class, new Renderer_RowSimple());
        //registerRowRenderer(Abstract_Row.class, new Renderer_RowText());
        //
        // Регистрируем рендереры по умолчанию для виджетов ячейки
        //registerCellRenderer(DataTypeInfo.ELogicTypes.String, new Renderer_CellText());
        registerCellRenderer(DataTypeInfo.ELogicTypes.String, new Renderer_CellPlainText());

        setLayout(new OverlayLayout(this));
        _LayerEdit = new Layer_Edit();
        _LayerEdit.setAlignmentX(SwingConstants.LEFT);
        _LayerEdit.setAlignmentY(SwingConstants.TOP);

        _LayerView = new Layer_View();
        _LayerView.setAlignmentX(SwingConstants.LEFT);
        _LayerView.setAlignmentY(SwingConstants.TOP);

        add(_LayerView, 0);

        _LayerView.add(_TableHeader, "cell 1 0, gapbottom 1px");
        _LayerView.add(_RowStructure, "cell 0 1, spanx 2");

        //_LayerView.add(Box.createRigidArea(new Dimension(30, 30)), "cell 0 0, gapright 1px");
        // Подключаем модель по умолчанию
        //setModel(defaultModel);
    }

    protected void selectTableCell(Widget_CellContainer cell)
    {
        if (_SelectedTableCell != null)
        {
            _SelectedTableCell.setSelected(false);
            _SelectedTableCell = null;
        }
        _SelectedTableCell = cell;

        if (_SelectedTableRow != null)
        {
            _SelectedTableRow.setSelected(false);
            _SelectedTableRow = null;
        }

        if (_SelectedTableCell != null)
        {
            _SelectedTableCell.setSelected(true);
        }
    }

    protected void selectTableRow(Row row)
    {
        if (_SelectedTableRow != null)
        {
            _SelectedTableRow.setSelected(false);
            _SelectedTableRow = null;
        }
        _SelectedTableRow = row;

        if (_SelectedTableCell != null)
        {
            _SelectedTableCell.setSelected(false);
            _SelectedTableCell = null;
        }

        if (_SelectedTableRow != null)
        {
            _SelectedTableRow.setSelected(true);
        }
    }

    protected Widget_TableWrap getOwnerTable()
    {
        return this;
    }

    @SuppressWarnings("unchecked")
    public void insertColumn(int index, int width, String columnCaption, IATOMName attributeName)
    {
        ITableView.IColumnView columnView = _DataView.createDefaultColumnInstance();
        columnView.setOwnerTable(_DataView);

        _DataView.addColumn("", DataTypeInfo.ELogicTypes.String);
        insertWidgetColumn(columnView);
    }

    private void insertWidgetColumn(ITableView.IColumnView columnView)
    {
        insertColumn(_Columns.size(), columnView);
    }

    /**
     * Вставка нового столбца в таблицу
     *
     * @param index Индекс, по которому необходимо добавить столбец
     * @param modelColumn Столбец модели данных
     */
    public void insertColumn(int index, ITable.IColumn modelColumn)
    {
        // Создание нового столбца таблицы и выдача ему рендерера по умолчанию
        Column column = new Column(this, modelColumn);

        if (modelColumn instanceof ITableView.IColumnView)
        {
            column.setAttributeName(((ITableView.IColumnView) modelColumn).getAttributeName());
            column.setCaption(((ITableView.IColumnView) modelColumn).getCaption());
        }

        column.setCellRenderer(_CellRenderers.get(modelColumn.getType()));

        _Columns.add(index, column);
        _TableHeader.addColumnHeader(column);

        for (int i = 0; i < _RowStructure.getRowCount(); i++)
        {
            Row row = _RowStructure.getRow(i);

            Widget_CellContainer newCell = new Widget_CellContainer(row);
            newCell.setModelCell(modelColumn.getCell(i));

            Widget_CellView newCellView = column.getCellRenderer().createViewWidget();
            newCellView.addMouseListener(new Listener_StartEditMode_MouseDoubleClicked());

            newCell.setViewWidget(newCellView);

        }
    }

    /**
     * Добавление нового столбца в таблицу
     *
     * @param modelColumn Столбец модели данных
     */
    public void addColumn(ITable.IColumn modelColumn)
    {
        int index = _Columns.size();
        insertColumn(index, modelColumn);
    }

    public Column getColumn(int index)
    {
        return _Columns.get(index);
    }

    public LinkedList<Column> getColumns()
    {
        return _Columns;
    }

    /**
     * Удаление столбца таблицы по индексу
     *
     * @param index Индекс столбца таблицы
     */
    public void removeColumn(int index)
    {
        _Columns.remove(index);
        this.remove(index);
    }

    /**
     * Вставка новой строки в таблицу
     *
     * @param index Индекс, по которому необходимо доавить ряд
     * @param modelRow Строка из модели данных
     */
    public void insertRow(int index, ITable.IRow modelRow)
    {

        Row row = _RowStructure.insertRow(index, modelRow);

        Widget_RowWidget_Cells rowWidget = (Widget_RowWidget_Cells) row.getRowWidget();

        for (int i = 0; i < _Columns.size(); i++)
        {
            Column column = _Columns.get(i);
            Widget_CellContainer newCell = new Widget_CellContainer(row);
            Widget_CellView newCellViewWidget = column.getCellRenderer().createViewWidget();

            newCellViewWidget.addMouseListener(new Listener_StartEditMode_MouseDoubleClicked());

            if ((index & 1) == 0)
            {
                newCellViewWidget.setBackgroundColor(new Color(220, 220, 220));
            } else
            {
                newCellViewWidget.setBackgroundColor(new Color(240, 240, 240));
            }

            newCell.setViewWidget(newCellViewWidget);
            newCell.setModelCell(modelRow.getCell(i));

            rowWidget.addCell(newCell);
        }
    }

    /**
     * Добавление новой строки в таблицу
     *
     * @param modelRow Строка из модели данных
     */
    public void addRow(ITable.IRow modelRow)
    {
        int index = _RowStructure.getRowCount();
        insertRow(index, modelRow);
    }

    /**
     * Удаление строки
     *
     * @param index Индекс строки на удаление
     */
    public void removeRow(int index)
    {
        _RowStructure.removeRow(index);
    }

    public LinkedList<RowHeader> getRowHeaders()
    {
        return _RowStructure.getRowHeaders();
    }

    /**
     * Регистрация нового рендерера для виджета табличного ряда
     *
     * @param key Ключ
     * @param newRenderer Рендерер
     */
    public void registerRowWidgetRenderer(Type key, IRenderer_RowTable newRenderer)
    {
        _RowStructure.registerRowWidgetRenderer(key, newRenderer);
    }

    /**
     * Регистрация рендерера по умолчанию для типа ячейки
     *
     * @param key Тип данных в ячейке
     * @param newRenderer Рендерер
     */
    private void registerCellRenderer(DataTypeInfo.ELogicTypes key, IRenderer_Cell newRenderer)
    {
        _CellRenderers.put(key, newRenderer);
    }

    @SuppressWarnings("unchecked")
    public final void setModelView(ITableView modelView)
    {
        _DataView = modelView;

        setModel(_DataView);
    }

    /**
     * Установка модели данных
     *
     * @param dataModel Модель данных
     */
    @SuppressWarnings("unchecked")
    public final void setModel(ITable dataModel)
    {
        _DataModel = dataModel;
        _DataModel.Delegate().AddListener(_ModelListener);
        _Columns.clear();
        //_RowStructure.clear();
//        _TableHeader.clear();

        // Заполняем таблицу в соответствии с моделью
        // Добавляем столбцы
        for (ITable.IColumn modelColumn : (LinkedList<ITable.IColumn>) _DataModel.getColumns())
        {
            addColumn(modelColumn);
        }
        // Добавляем ряды
        for (ITable.IRow modelRow : (LinkedList<ITable.IRow>) _DataModel.getRows())
        {
            //System.out.println(modelRow.getCell(2).getValue());
            addRow(modelRow);
        }
    }

    /**
     * Временный метод для доступа к модели данных, подключенной к виджету
     *
     * @return Модель данных
     */
    public ITable getModel()
    {
        return _DataModel;
    }

    /**
     * Метод, запускающий режим редактирования ячейки таблицы
     *
     * @param cell Ячейка таблицы
     * @param cellEdit
     */
    protected void startEditMode(Widget_CellContainer cell, Widget_CellEdit cellEdit)
    {
        _LayerEdit.setEditWidget(this, cell, cellEdit);
        add(_LayerEdit, 0);
        _LayerEdit.requestFocus();

        revalidateWidget();
    }

    /**
     * Метод, завершающий режим редактирования ячейки
     */
    protected void stopEditMode()
    {
        remove(_LayerEdit);

        revalidateWidget();
    }

    /**
     * Пересчет и перерисовка виджета таблицы
     */
    protected void revalidateWidget()
    {
        revalidate();
        repaint();
    }

    protected void updateColumnWidth(Column column)
    {
        int index = _Columns.indexOf(column);

        for (int i = 0; i < _RowStructure.getRowCount(); i++)
        {
            Row row = _RowStructure.getRow(i);
            Widget_RowWidget_Cells rowWidget = (Widget_RowWidget_Cells) row.getRowWidget();

            rowWidget.getCell(index).setWidth(column.getWidth());
            rowWidget.setPreferredSize(new Dimension(_TableHeader.getPreferredSize().width - 34, row.getHeight()));
        }
    }

    public JComponent createDefaultTableNestedWidget(Row row)
    {
        Widget_TableNested nestedTable = new Widget_TableNested();
        nestedTable.setOwnerTable(getOwnerTable());
        nestedTable.setModel(row.getModelRow().getNestedTable());

        return nestedTable;
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.GRAY);
        g2.fillRect(0, 0, getWidth(), getHeight());
    }

    private class Wrap_RowStructure extends Widget_RowStructure
    {

        @Override
        public IATOMEventDelegate Delegate()
        {
            return getOwnerTable().Delegate();
        }

        @Override
        public void selectRow(Row row)
        {
            selectTableRow(row);
        }

        @Override
        public int getPreferredWidth()
        {
            return _TableHeader.getPreferredSize().width - 34;
        }

        @Override
        public JComponent createDefaultNestedWidget(Row row)
        {
            return createDefaultTableNestedWidget(row);
        }
    }

    /**
     * Класс слоя для отображения таблицы
     */
    protected class Layer_View extends JComponent
    {

        public Layer_View()
        {
            setLayout(new MigLayout("flowy", "0[pref]0", "0[pref]0"));
        }

    }

    /**
     * Класс слоя для отображения режима редактирования ячейки
     */
    private class Layer_Edit extends JPanel
    {

        Widget_CellContainer _Cell;
        Widget_CellEdit _CellEdit;

        public Layer_Edit()
        {
            setBackground(new Color(10, 10, 10, 210));
            setLayout(new MigLayout("", "0[pref]0", "0[pref]0"));
        }

        /**
         * Установка виджета редактирования ячейки
         *
         * @param ownerWidget Таблица - владелец
         * @param cell Ячейка
         */
        public void setEditWidget(Widget_TableWrap ownerWidget, Widget_CellContainer cell, Widget_CellEdit cellEdit)
        {
            _Cell = cell;
            _CellEdit = cellEdit;

            Point p = SwingUtilities.convertPoint(cell, 0, 0, ownerWidget);

            addMouseListener(new Listener_StopEditMode_MousePressed());

            _CellEdit.setInitValue(_Cell.getValue());
            _CellEdit.addKeyListener(new Listener_StopEditMode_KeyPressed());

            add(Box.createRigidArea(new Dimension(p.x, 0)), "cell 0 1, growy");
            add(Box.createRigidArea(new Dimension(0, p.y)), "cell 1 0, growx");
            add(_CellEdit, "cell 1 1, grow");

            revalidate();
            repaint();
        }

        /**
         * Установка фокуса на виджет редактирования
         */
        @Override
        public void requestFocus()
        {
            _CellEdit.requestFocus();
        }

        /**
         * Сохранение изменений
         */
        public void saveEditMode()
        {
            _Cell.setValue(_CellEdit.getNewValue());
            //updateColumnMinimumWidth(_Columns.get(_Cell.getColumnIndex()));

            removeAll();

            revalidate();
            repaint();
        }

        /**
         * Отмена изменений
         */
        public void discardEditMode()
        {
            removeAll();

            revalidate();
            repaint();
        }
    }

    public void setHeaderVisible(boolean flag)
    {

    }

    public void updateColumnMinimumWidth(Column column)
    {
        int minimumWidth = 0;

        for (Row row : _RowStructure.getRows())
        {
            Widget_RowWidget_Cells rowSimple = (Widget_RowWidget_Cells) row.getRowWidget();
            Widget_CellContainer cell = rowSimple.getCell(column.getIndex());

            if (cell.getMinimumWidth() > minimumWidth)
            {
                minimumWidth = cell.getMinimumWidth();
            }
        }

        column.setMinimumWidth(minimumWidth);
    }

    private class Listener_StartEditMode_MouseDoubleClicked extends MouseAdapter
    {

        boolean isAlreadyOneClick;

        @Override
        public void mousePressed(MouseEvent e)
        {
            Widget_CellContainer cell = (Widget_CellContainer) ((JComponent) e.getSource()).getParent().getParent();
            Events.CellSelected.Rise(getOwnerTable(), cell);
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            if (isAlreadyOneClick)
            {
                Widget_CellContainer cell = (Widget_CellContainer) ((JComponent) e.getSource()).getParent().getParent();
                Widget_CellEdit cellEdit = getColumn(cell.getColumnIndex()).getCellRenderer().createEditWidget();

                startEditMode(cell, cellEdit);
                isAlreadyOneClick = false;
            } else
            {
                isAlreadyOneClick = true;
                Timer t = new Timer("doubleclickTimer", false);
                t.schedule(new TimerTask()
                {

                    @Override
                    public void run()
                    {
                        isAlreadyOneClick = false;
                    }
                }, 300);
            }
        }
    }

    private class Listener_StopEditMode_MousePressed extends MouseAdapter
    {

        @Override
        public void mousePressed(MouseEvent e)
        {
            _LayerEdit.saveEditMode();
            stopEditMode();
        }
    }

    private class Listener_StopEditMode_KeyPressed extends KeyAdapter
    {

        @Override
        public void keyPressed(KeyEvent e)
        {
            switch (e.getKeyCode())
            {
                case KeyEvent.VK_ESCAPE:
                    _LayerEdit.discardEditMode();
                    stopEditMode();
                    break;
                case KeyEvent.VK_ENTER:
                    _LayerEdit.saveEditMode();
                    stopEditMode();
                    break;
                default:
                    revalidateWidget();
                    break;
            }
        }
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    /**
     * Делегат событий класса
     */
    //protected IATOMEventDelegate _Delegate = new ATOMEventDelegate(); // для классов без собственной логики событий
    protected IATOMEventDelegate _Delegate = new EventLogic(); // Для класса с собственной логикой событий

    @Override
    public IATOMEventDelegate Delegate()
    {
        return _Delegate;              //Альтернатива 1. Класс ведет собственный учет слушателей для генерируемых им событий
        //                и/или имеет логику обработки собственных генерируемых событий
        //return _OwnerManager.Delegate();  //Альтернатива 2. Класс полностью перенаправляет учет, генерацию событий и обработку логики в управляющий класс
        // Используется, когда класс может генерировать события, но в программе создается большое число
        // экземпляров классов  при редких индивидуальных подписчиках. Позволяет иключить необходимость
        // подписываться управляющему классу к каждому подчиненному экземпляру подчиненного класса.
        // Пример 1. Класс "Ячейка в таблице" может перенаправлять учет и обработку событий в
        // класс "Строка таблицы", которая в свою очередь может перенаправлять управление в
        // класс "Таблица".
        // Пример 2. Класс "Атрибут сущности данных" может перенаправлять учет и обработку событий
        // в класс "Сущность данных".

    }

    /*Логика обработки событий атрибута*/
    @Override
    public IATOMEventLogic EventLogic()
    {
        return _Delegate;
    }

    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ATOMEvent
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super(eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 1
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Выделение ячейки" #####">
        /**
         * Класс экземпляра события ""
         */
        public static class CellSelected extends Events
        {

            /**
             * Тип события ""
             */
            protected final static IATOMName _CellSelected = new ATOMName("EventSource.Events.CellSelected");

            //Параметры события
            public Widget_CellContainer SelectedCell;

            public CellSelected(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public CellSelected(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _CellSelected;
            }

            protected static CellSelected Create(IATOMEventDelegateOwner eventSource, Object eventTarget, Widget_CellContainer cell)
            {
                CellSelected e = new CellSelected(eventSource, eventTarget);
                e.SelectedCell = cell;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".* @param eventSource
             * Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Widget_TableWrap eventSource, Widget_CellContainer cell)
            {
                CellSelected e = Create(eventSource, eventSource, cell);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ """
//##################################################################

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 2
    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMEventDelegate        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.CellSelected.class.isAssignableFrom(eventClass))
            {
                Events.CellSelected event = (Events.CellSelected) e;
                isAllowEvent = OnBefore_CellSelected(event);
            }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.CellSelected.class.isAssignableFrom(eventClass))
            {
                Events.CellSelected event = (Events.CellSelected) e;
                isDone = OnDo_CellSelected(event);
            }

            //2. Обработчик события Events.Event2
            if (Widget_RowStructure.Events.RowSelected.class.isAssignableFrom(eventClass))
            {
                Widget_RowStructure.Events.RowSelected event = (Widget_RowStructure.Events.RowSelected) e;
                isDone = ((Widget_RowStructure) event.getEventSource()).EventLogic().OnDoEvent(eventSource, e);
            }

            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

    private boolean OnBefore_CellSelected(Events.CellSelected event)
    {
        return true;
    }

    private boolean OnDo_CellSelected(Events.CellSelected event)
    {
        selectTableCell(event.SelectedCell);

        return true;
    }
//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="##### УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ #####">
    protected class CommonListener extends ATOMEventListenerAdapter
    {

        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            // Обработка события изменения значения в ячейке
            if (ITable.ICell.Events.ValueChange.class.isAssignableFrom(eventClass))
            {
                ITable.ICell.Events.ValueChange event = (ITable.ICell.Events.ValueChange) e;
                isDone = OnDo_ValueChange(event);
            }

            return isDone;
        }

        private boolean OnDo_ValueChange(ITable.ICell.Events.ValueChange event)
        {
            int rowIndex = event.Cell.getRowIndex();
            int columnIndex = event.Cell.getColumnIndex();

            Widget_CellContainer cell = ((Widget_RowWidget_Cells) _RowStructure.getRow(rowIndex).getRowWidget()).getCell(columnIndex);
            cell.update();
            return true;
        }
    }
//</editor-fold> КОНЕЦ "УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ"
//##################################################################
}
