package atom.components.Table2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author Добровольский
 */
public class Widget_ColumnHeader_View extends JLabel
{

    public Widget_ColumnHeader_View()
    {

        setBorder(BorderFactory.createEmptyBorder());

        setHorizontalAlignment(SwingConstants.CENTER);
        setPreferredSize(new Dimension(150, 30));
    }

    @Override
    public void setText(String text)
    {
        super.setText("<html><b>" + text + "</b></html>");
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(new Color(180, 217, 241));
        g2.fillRect(0, 0, getWidth(), getHeight());

        super.paintComponent(g);
    }
}
