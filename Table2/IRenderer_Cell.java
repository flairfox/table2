package atom.components.Table2;

import javax.swing.JComponent;

/**
 * Интерфейс для подключения рендереров ячейки
 *
 * @author Добровольский
 */
public interface IRenderer_Cell
{

    /**
     * Создание виджета отображения для ячейки таблицы
     *
     * @return Виджет отображения ячейки
     */
    public Widget_CellView createViewWidget();
    
    /**
     * Создание виджета редактирования для ячейки таблицы
     * 
     * @return виджет редактирования
     */
    public Widget_CellEdit createEditWidget();
}
