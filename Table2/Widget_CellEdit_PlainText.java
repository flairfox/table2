package atom.components.Table2;

import java.awt.Dimension;
import java.awt.event.KeyListener;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Добровольский
 */
public class Widget_CellEdit_PlainText extends Widget_CellEdit
{

    JScrollPane _ScrollPane;
    JTextArea _TextArea;

    public Widget_CellEdit_PlainText()
    {
        _TextArea = new JTextArea();
        _ScrollPane = new JScrollPane(_TextArea);
        
        add(_ScrollPane, "cell 0 0");
        //_TextArea.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        _ScrollPane.setMinimumSize(new Dimension(250, 100));
        _ScrollPane.setMaximumSize(new Dimension(250, 300));

        _TextArea.setLineWrap(true);
        _TextArea.setWrapStyleWord(true);

        //JButton button = new JButton("I do nothing");
        //add(button, "cell 0 1, gaptop 1px, growx");
    }

    @Override
    public void setInitValue(Object value)
    {
        if (value != null)
        {
            _TextArea.setText(value.toString());
        } else
        {
            _TextArea.setText("");
        }

    }

    @Override
    public Object getNewValue()
    {
        return _TextArea.getText();
    }

    @Override
    public void addKeyListener(KeyListener l)
    {
        _TextArea.addKeyListener(l);
    }

    @Override
    public void requestFocus()
    {
        _TextArea.requestFocus();
        _TextArea.selectAll();
    }
}
