/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atom.components.Table2;

/**
 *
 * @author Vadim
 */
public class Renderer_CellPlainText implements IRenderer_Cell
{

    @Override
    public Widget_CellView createViewWidget()
    {
        return new Widget_CellView_Text();
    }

    @Override
    public Widget_CellEdit createEditWidget()
    {
        return new Widget_CellEdit_PlainText();
    }
    
}
