package atom.components.Table2;

import java.util.LinkedList;
import javax.swing.JComponent;
import net.miginfocom.swing.MigLayout;

/**
 * Строковая организация данных
 *
 * @author Добровольский
 */
public class Widget_RowList extends JComponent
{

    private LinkedList<Widget_RowContainer> _Rows;

    public Widget_RowList()
    {
        setLayout(new MigLayout("flowy", "0[pref]0[pref]0", "0[pref]1[pref]0"));

        _Rows = new LinkedList<Widget_RowContainer>();
    }

    public void insertRow(int index, Row newRow)
    {
        Widget_RowContainer newWidgetRow = newRow.getWidgetRow();
        _Rows.add(index, newWidgetRow);
        add(newWidgetRow, index);
    }

    public void removeRow(int index)
    {
        remove(_Rows.get(index));
        _Rows.remove(index);
    }

    public void clear()
    {
        removeAll();
        _Rows.clear();
    }
    
    public Widget_RowContainer getRow(int index)
    {
        return _Rows.get(index);
    }
}
