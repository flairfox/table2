package atom.components.Table2;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * Поле отображения текстовой информации
 *
 * @author Добровольский
 */
public class Widget_CellView_Text extends Widget_CellView
{

    private JLabel _Label;

    public Widget_CellView_Text()
    {
        _Label = new JLabel();
        _BackgroundColor = Color.WHITE;

        Border line = BorderFactory.createEmptyBorder();
        Border empty = new EmptyBorder(0, 3, 0, 3);
        CompoundBorder border = new CompoundBorder(line, empty);
        _Label.setBorder(border);
        _Label.setHorizontalAlignment(SwingConstants.LEFT);
        _Label.setVerticalAlignment(SwingConstants.CENTER);
        
        _Label.setFont(_Label.getFont().deriveFont(Font.PLAIN));

        add(_Label, "grow");
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(_BackgroundColor);

        g2.fillRect(0, 0, getWidth(), getHeight());

        super.paintComponent(g);
    }

    @Override
    public void updateValue(Object value)
    {
        if (value != null)
        {
            _Label.setText("<html>" + value.toString() + "</html>");
        } else
        {
            _Label.setText("<html></html>");
        }
    }

    @Override
    public void addMouseListener(MouseListener l)
    {
        _Label.addMouseListener(l);
    }
    
    @Override
    public Color getBackgroundColor()
    {
        return _BackgroundColor;
    }

    @Override
    public void setBackgroundColor(Color color)
    {
        _BackgroundColor = color;
        repaint();
    }
    
    @Override
    public Color getTextColor()
    {
        return _Label.getForeground();
    }
    
    @Override
    public void setTextColor(Color color)
    {
        _Label.setForeground(color);
    }
    
    @Override
    public void setTextFont(Font font)
    {
        _TextFont = font;
        _Label.setFont(font);
    }

    @Override
    public Font getTextFont()
    {
        return _Label.getFont();
    }

    @Override
    public int getMinimumWidth()
    {
        if (_Label.getMinimumSize().width < 150)
        {
            return 150;
        } else
        {
            return _Label.getMinimumSize().width;
        }
    }

    @Override
    public int getDefaulWidth()
    {
        return 150;
    }
}
