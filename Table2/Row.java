package atom.components.Table2;

import atom.data.TableModel2.ITable;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JComponent;

/**
 *
 * @author Добровольский
 */
public class Row
{

    private Widget_RowStructure _OwnerStructure;

    private ITable.IRow _ModelRow;

    private Widget_RowContainer _WidgetRow;
    private Widget_RowHeaderContainer _WidgetRowHeader;

    private RowHeader _RowHeader;
    
    private Color _BackgroundColor;
    private Color _TextColor;

    private int _Height;

    private boolean _isExpandable, _isExpanded;

    public Row(Widget_RowStructure ownerStructure)
    {
        _OwnerStructure = ownerStructure;

        _Height = 30;
        _isExpandable = false;
        _isExpanded = false;

        _RowHeader = new RowHeader(this);

        _WidgetRow = new Widget_RowContainer();
        _WidgetRowHeader = new Widget_RowHeaderContainer();
    }
    
    public void setExpandable(boolean flag)
    {
        _isExpandable = flag;
        _WidgetRowHeader.setExpandable(flag);
    }
    
    public void setBackgroundColor(Color color)
    {
        _BackgroundColor = color;
        _WidgetRow.setBackgroundColor(color);
    }
    
    public Color getBackgroundColor()
    {
        return _BackgroundColor;
    }

    public void setTextColor(Color color)
    {
        _TextColor = color;
        _WidgetRow.setTextColor(color);
    }

    public Color getTextColor()
    {
        return _TextColor;
    }
    
    public boolean isExpandable()
    {
        return _isExpandable;
    }
    
    public void setSelected(boolean flag)
    {
        _WidgetRow.setSelected(flag);
        _WidgetRowHeader.setSelected(flag);
    }
    
    public void setModelRow(ITable.IRow modelRow)
    {
        _ModelRow = modelRow;
    }

    public ITable.IRow getModelRow()
    {
        return _ModelRow;
    }

    public void setRowHeader(Widget_RowHeader_View rowHeaderView)
    {
        _WidgetRowHeader.setMainWidget(rowHeaderView);
        _RowHeader.setRowHeaderView(rowHeaderView);
    }

    public void setRowWidget(JComponent rowWidget)
    {
        rowWidget.addComponentListener(new Listener_RowResize());
        _WidgetRow.setMainWidget(rowWidget);
    }

    public void setNestedWidget(JComponent nestedWidget)
    {
        _WidgetRow.setNestedWidget(nestedWidget);
        _RowHeader.setReferenceWidget(nestedWidget);
    }

    public JComponent getRowWidget()
    {
        return _WidgetRow.getMainWidget();
    }

    public Widget_RowContainer getWidgetRow()
    {
        return _WidgetRow;
    }

    public Widget_RowHeaderContainer getWidgetRowHeader()
    {
        return _WidgetRowHeader;
    }

    public RowHeader getRowHeader()
    {
        return _RowHeader;
    }

    public JComponent getNestedWidget()
    {
        return _WidgetRow.getNestedWidget();
    }

    public void setStructureMarkers(JComponent structureMarkers)
    {
        _WidgetRowHeader.setNestedWidget(structureMarkers);
    }

    public void setHeight(int height)
    {
        int w = _OwnerStructure.getPreferredWidth();

        _Height = height;

        _RowHeader.setHeight(height);
        _WidgetRowHeader.getMainWidget().setPreferredSize(new Dimension(30, height));

        _WidgetRow.getMainWidget().setPreferredSize(new Dimension(w, height));
        _WidgetRow.revalidate();
    }

    public int getHeight()
    {
        return _Height;
    }

    public void changeState()
    {
        if (_isExpanded)
        {
            _WidgetRow.collapse();
            _WidgetRowHeader.collapse();
            _RowHeader.collapse();
        } else
        {
            _WidgetRow.expand();
            _WidgetRowHeader.expand();
            _RowHeader.expand();
        }

        ((Widget_StructureMarkers_Table) _WidgetRowHeader.getNestedWidget()).update();
        _RowHeader.updateHeight(_WidgetRow.getNestedWidget().getPreferredSize().height);
        _isExpanded = !_isExpanded;
    }

    protected void updateSize()
    {
        _WidgetRowHeader.getMainWidget().setPreferredSize(new Dimension(30, _WidgetRow.getMainWidget().getPreferredSize().height));
        _WidgetRowHeader.revalidate();
    }

    private class Listener_RowResize extends ComponentAdapter
    {

        @Override
        public void componentResized(ComponentEvent e)
        {
            updateSize();
        }
    }
}
