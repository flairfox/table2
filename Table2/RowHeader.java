package atom.components.Table2;

import java.awt.event.ActionListener;
import javax.swing.JComponent;

/**
 *
 * @author Добровольский
 */
public class RowHeader
{

    private Row _OwnerRow;

    private Widget_RowHeader_View _RowHeaderView;
    private Widget_RowHeader_Controller _RowHeaderController;

    public RowHeader(Row ownerRow)
    {
        _OwnerRow = ownerRow;
    }
    
    public void setReferenceWidget(JComponent referenceWidget)
    {
        _RowHeaderController.setReferenceWidget(referenceWidget);
    }

    public void updateHeight(int height)
    {
        _RowHeaderController.updateHeight();

    }

    public void expand()
    {
        _RowHeaderView.changeState();
        _RowHeaderController.expand();
    }

    public void collapse()
    {
        _RowHeaderView.changeState();
        _RowHeaderController.collapse();
    }

    public void setRowHeaderController(Widget_RowHeader_Controller rowHeaderController)
    {
        _RowHeaderController = rowHeaderController;
    }

    public void setRowHeaderView(Widget_RowHeader_View rowHeaderView)
    {
        _RowHeaderView = rowHeaderView;
    }

    public Widget_RowHeader_View getRowHeaderView()
    {
        return _RowHeaderView;
    }

    public void setRowIndex(int index)
    {
        _RowHeaderView.setText(Integer.toString(index));
    }

    public void setExpandable(boolean flag)
    {
        _RowHeaderView.setExpandable(flag);
    }

    public void addActionListener(ActionListener l)
    {
        _RowHeaderView.addActionListener(l);
    }

    public void setHeight(int height)
    {
        _RowHeaderController.setHeight(height);
    }
}
