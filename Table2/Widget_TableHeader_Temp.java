package atom.components.Table2;

import java.util.LinkedList;
import javax.swing.JComponent;
import javax.swing.OverlayLayout;
import net.miginfocom.swing.MigLayout;

/**
 * Заголовок таблицы
 *
 * @author Доброволський
 */
public class Widget_TableHeader_Temp extends JComponent
{

    protected Widget_TableWrap _OwnerTable;

    protected TableHeader_Views _TableHeader_Views;
    protected TableHeader_Controllers _TableHeader_Controllers;

    private LinkedList<ColumnHeader> _ColumnHeaders;

    public Widget_TableHeader_Temp(Widget_TableWrap ownerTable)
    {
        setLayout(new OverlayLayout(this));

        _OwnerTable = ownerTable;

        _TableHeader_Views = new TableHeader_Views();
        _TableHeader_Controllers = new TableHeader_Controllers();

        _ColumnHeaders = new LinkedList<ColumnHeader>();

        add(_TableHeader_Views, 0);
        add(_TableHeader_Controllers, 0);
    }

    public void addColumnHeader(Column column)
    {
        ColumnHeader columnHeader = new ColumnHeader(column);

        _ColumnHeaders.add(columnHeader);
        _TableHeader_Views.add(columnHeader.getColumnHeaderView());
        _TableHeader_Controllers.addColumnHeaderController(columnHeader.getColumnHeaderController());
        
        revalidate();
    }

    private class TableHeader_Views extends JComponent
    {
        private LinkedList<Widget_ColumnHeader_View> _ColumnHeaderViews;

        public TableHeader_Views()
        {
            setLayout(new MigLayout("flowx", "0[pref]1[pref]0", "0[pref]0[pref]0"));
            
            _ColumnHeaderViews = new LinkedList<Widget_ColumnHeader_View>();
        }
        
        public void addColumnHeaderView(Widget_ColumnHeader_View columnHeaderView)
        {
            _ColumnHeaderViews.add(columnHeaderView);
            add(columnHeaderView);
            revalidate();
        }
    }

    private class TableHeader_Controllers extends JComponent
    {
        private LinkedList<Widget_ColumnHeader_Controller> _ColumnHeaderControllers;

        public TableHeader_Controllers()
        {
            setLayout(new MigLayout("flowx", "0[pref]0", "0[pref]0"));
            
            _ColumnHeaderControllers = new LinkedList<Widget_ColumnHeader_Controller>();
        }
        
        public void addColumnHeaderController(Widget_ColumnHeader_Controller columnHeaderController)
        {
            _ColumnHeaderControllers.add(columnHeaderController);
            add(columnHeaderController);
            revalidate();
        }
    }

    private class Spacer extends JComponent
    {

    }
}
