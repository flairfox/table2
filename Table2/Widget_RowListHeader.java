package atom.components.Table2;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.OverlayLayout;
import javax.swing.event.MouseInputAdapter;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Доброволський
 */
public class Widget_RowListHeader extends JComponent
{

    protected Widget_RowStructure _OwnerStructure;

    private RowListHeader_Views _RowListHeader_Views;
    private RowListHeader_Controllers _RowListHeader_Controllers;

    private LinkedList<RowHeader> _RowHeaders;

    public Widget_RowListHeader(Widget_RowStructure ownerStructure)
    {
        setLayout(new OverlayLayout(this));

        _OwnerStructure = ownerStructure;

        _RowListHeader_Views = new RowListHeader_Views();
        _RowListHeader_Controllers = new RowListHeader_Controllers();

        _RowHeaders = new LinkedList<RowHeader>();

        add(_RowListHeader_Views, 0);
        add(_RowListHeader_Controllers, 0);
    }

    public LinkedList<RowHeader> getRowHeaders()
    {
        return _RowHeaders;
    }

    public void insertRowHeader(int index, Row row)
    {
        RowHeader rowHeader = row.getRowHeader();

        _RowHeaders.add(rowHeader);
        _RowListHeader_Views.addRowHeaderView(row.getWidgetRowHeader());

        Widget_RowHeader_Controller rowHeaderController = new Widget_RowHeader_Controller();
        rowHeaderController.setHeight(row.getHeight());

        rowHeader.setRowHeaderController(rowHeaderController);
        _RowListHeader_Controllers.addRowHeaderController(rowHeaderController);
    }

    public void enumerateRows()
    {
        for (int i = 0; i < _RowHeaders.size(); i++)
        {
            _RowHeaders.get(i).setRowIndex(i);
        }
    }

    private class RowListHeader_Views extends JComponent
    {

        private LinkedList<Widget_RowHeaderContainer> _RowHeaderViews;

        public RowListHeader_Views()
        {
            setLayout(new MigLayout("flowy", "0[pref]0[pref]0", "0[pref]1[pref]0"));

            _RowHeaderViews = new LinkedList<Widget_RowHeaderContainer>();
        }

        public void addRowHeaderView(Widget_RowHeaderContainer rowHeaderView)
        {
            _RowHeaderViews.add(rowHeaderView);
            add(rowHeaderView, "grow");
            revalidate();
        }
    }

    private class RowListHeader_Controllers extends JComponent
    {

        private LinkedList<Widget_RowHeader_Controller> _RowHeaderControllers;
        private Listener_RowHeightControllers _RowHeightListener;

        public RowListHeader_Controllers()
        {
            setLayout(new MigLayout("flowy", "0[pref]0[pref]0", "0[pref]0[pref]0"));

            add(Box.createRigidArea(new Dimension(0, 2)));

            _RowHeaderControllers = new LinkedList<Widget_RowHeader_Controller>();
            _RowHeightListener = new Listener_RowHeightControllers(this, _OwnerStructure);
        }

        public void addRowHeaderController(Widget_RowHeader_Controller rowHeaderController)
        {
            rowHeaderController.addMouseListener(_RowHeightListener);
            _RowHeaderControllers.add(rowHeaderController);
            add(rowHeaderController, "grow");
            revalidate();
        }
    }

    private class Listener_RowHeightControllers extends MouseInputAdapter
    {

        int _yPressed, _PrevHeight, _NewHeight;
        protected Widget_RowListHeader.RowListHeader_Controllers _HeightControllers;
        protected Widget_RowHeader_Controller _HeightController;
        protected Widget_RowStructure _OwnerStructure;
        Container _Parent;

        public Listener_RowHeightControllers(Widget_RowListHeader.RowListHeader_Controllers heightControllers, Widget_RowStructure ownerStructure)
        {
            _HeightControllers = heightControllers;
            _OwnerStructure = ownerStructure;
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            _yPressed = e.getYOnScreen();

            _HeightController = (Widget_RowHeader_Controller) ((JComponent) e.getSource()).getParent().getParent();
            _PrevHeight = _OwnerStructure.getRow(_HeightControllers._RowHeaderControllers.indexOf(_HeightController)).getHeight();

            _Parent = (Container) _HeightControllers;
            while (!(_Parent instanceof JFrame))
            {
                _Parent = _Parent.getParent();
            }

            _Parent.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            _Parent.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }

        @Override
        public void mouseDragged(MouseEvent e)
        {
            _NewHeight = _PrevHeight + (e.getYOnScreen() - _yPressed);

            _OwnerStructure.getRow(_HeightControllers._RowHeaderControllers.indexOf(_HeightController)).setHeight(_NewHeight);
        }
    }
}
