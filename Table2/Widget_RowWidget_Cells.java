package atom.components.Table2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.LinkedList;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;
import net.miginfocom.swing.MigLayout;

/**
 * Представление данных в виде ряда, состоящего из набора ячеек
 *
 * @author Добровольский
 */
public class Widget_RowWidget_Cells extends JComponent
{

    /**
     * Коллекция ячеек
     */
    private LinkedList<Widget_CellContainer> _Cells;

    private boolean _isSelected;

    /**
     * Инициализация
     */
    public Widget_RowWidget_Cells()
    {
        _isSelected = false;
        _Cells = new LinkedList<Widget_CellContainer>();
        this.setLayout(new MigLayout("fill", "0[pref]1[pref]0", "0[pref]0"));
    }

    /**
     * Данный метод при наследовании необходимо переопределить
     *
     * @param newCell Новая ячейка
     */
    public void addCell(Widget_CellContainer newCell)
    {
        _Cells.add(newCell);
        this.add(newCell, "grow");
    }

    public Widget_CellContainer getCell(int index)
    {
        return _Cells.get(index);
    }

    public LinkedList<Widget_CellContainer> getCells()
    {
        return _Cells;
    }

    public void setBackgroundColor(Color color)
    {
        for (Widget_CellContainer cell : _Cells)
        {
            cell.setBackgroundColor(color);
        }
    }
    
    public void setTextColor(Color color)
    {
        for (Widget_CellContainer cell : _Cells)
        {
            cell.setTextColor(color);
        }
    }

    @Override
    public void paintChildren(Graphics g)
    {
        super.paintChildren(g);

        if (_isSelected)
        {
            Graphics2D g2 = (Graphics2D) g;

            g2.setColor(new Color(30, 119, 162));
            g2.setStroke(new BasicStroke(4));

            g2.drawLine(0, 0, getWidth(), 0);
            g2.drawLine(getWidth(), 0, getWidth(), getHeight());
            g2.drawLine(0, getHeight(), getWidth(), getHeight());
        }
    }

    public void setSelected(boolean flag)
    {
        _isSelected = flag;
        repaint();
    }
}
