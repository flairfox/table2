package atom.components.Table2;

import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author Доброволський
 */
public class ColumnHeader
{

    private Column _OwnerColumn;

    private Widget_ColumnHeader_View _ColumnHeaderView;
    private Widget_ColumnHeader_Controller _ColumnHeaderController;

    public ColumnHeader(Column ownerColumn)
    {
        _OwnerColumn = ownerColumn;

        _ColumnHeaderView = new Widget_ColumnHeader_View();
        _ColumnHeaderView.setText(_OwnerColumn.getName() + " : " + _OwnerColumn.getRole());

        _ColumnHeaderController = new Widget_ColumnHeader_Controller();
    }

    public Widget_ColumnHeader_View getColumnHeaderView()
    {
        return _ColumnHeaderView;
    }

    public Widget_ColumnHeader_Controller getColumnHeaderController()
    {
        return _ColumnHeaderController;
    }

    public void addMouseListener(MouseInputAdapter l)
    {
        _ColumnHeaderController.addMouseListener(l);
    }
}
