package atom.components.Table2;

import javax.swing.JComponent;

/**
 * Рендерер текстовой ячейки 
 * 
 * @author Добровольский
 */
public class Renderer_CellText implements IRenderer_Cell
{
    @Override
    public Widget_CellView createViewWidget()
    {
        return new Widget_CellView_Text();
    }

    @Override
    public Widget_CellEdit createEditWidget()
    {
        return new Widget_CellEdit_Text();
    }
}
