package atom.components.Table2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

/**
 *
 * @author Добровольский
 */
public class Widget_StructureMarkers_Table extends JComponent
{

    private JComponent _NestedStructure;
    private Rectangle _CurrentRectangle;
    private boolean _isHighlighted, _isScrolledUp;

    public void setNestedStructure(JComponent nestedStructure)
    {
        _isHighlighted = false;
        _isScrolledUp = false;

        _NestedStructure = nestedStructure;

        Listener_OwnerStructureResize resizeListener = new Listener_OwnerStructureResize();
        _NestedStructure.addComponentListener(resizeListener);

        Listener_HighlightStructure highlightListener = new Listener_HighlightStructure();
        addMouseListener(highlightListener);
    }

    public Rectangle getOwnerRowRectangle()
    {
        Widget_TableWrap table = (Widget_TableWrap) _NestedStructure;
        Widget_RowHeaderContainer rowHeader = (Widget_RowHeaderContainer) getParent();

        int x = rowHeader.getMainWidget().getBounds().x;
        int y = rowHeader.getMainWidget().getBounds().y;
        int w = 0;
        int h = 0;

        return SwingUtilities.convertRectangle(rowHeader, new Rectangle(x, y, w, h), table);
    }

    public Rectangle getCurrentRectangle()
    {
        Widget_TableWrap table = (Widget_TableWrap) _NestedStructure;

        int x = getVisibleRect().x;
        int y = getVisibleRect().y;
        int w = 0;
        int h = 0;

        return SwingUtilities.convertRectangle(this, new Rectangle(x, y, w, h), table);
    }

    public void update()
    {
        setPreferredSize(new Dimension(30, _NestedStructure.getPreferredSize().height));
        revalidate();
    }

    protected void drawStructureMarks(Graphics2D g2)
    {
        if (_isHighlighted)
        {
            g2.setColor(new Color(180, 217, 241));
        } else
        {
            g2.setColor(Color.DARK_GRAY);
        }

        Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]
        {
            1
        }, 0);
        g2.setStroke(dashed);

        Widget_TableWrap table = (Widget_TableWrap) _NestedStructure;

        int x = getWidth() / 2;
        int yPrev = 0;

        for (RowHeader rowHeader : table.getRowHeaders())
        {
            int y = SwingUtilities.convertPoint(rowHeader.getRowHeaderView(), 0, 0, this).y + rowHeader.getRowHeaderView().getHeight() / 2;

            g2.drawLine(x, yPrev, x, y);
            g2.drawLine(x, y, 2 * x + 1, y);

            yPrev = y;
        }
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        drawStructureMarks(g2);
    }

    private class Listener_OwnerStructureResize extends ComponentAdapter
    {

        @Override
        public void componentResized(ComponentEvent e)
        {
            update();
        }
    }

    private class Listener_HighlightStructure extends MouseAdapter
    {

        @Override
        public void mouseEntered(MouseEvent e)
        {
            _isHighlighted = true;
            repaint();
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
            _isHighlighted = false;
            repaint();
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            Widget_TableWrap table = (Widget_TableWrap) _NestedStructure;

            if (_isScrolledUp)
            {
                table.getOwnerTable().scrollRectToVisible(_CurrentRectangle);
                _isScrolledUp = false;
            } else
            {
                _CurrentRectangle = table.getOwnerTable().getVisibleRect();

                scrollRectToVisible(getOwnerRowRectangle());
                _isScrolledUp = true;
            }
        }
    }
}
