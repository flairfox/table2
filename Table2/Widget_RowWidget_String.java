package atom.components.Table2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JTextField;

/**
 * Текстовая строка
 *
 * @author Добровольский
 */
public class Widget_RowWidget_String extends Widget_RowWidget_Cells
{

    JTextField _TextField;

    public Widget_RowWidget_String()
    {
        super();

        // Настраиваем текстовое поле
        _TextField = new JTextField();
        _TextField.setText("Some text row...");
        _TextField.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        _TextField.setPreferredSize(new Dimension(600, 30));
        
        this.add(_TextField, "grow");
    }

    @Override
    public void addCell(Widget_CellContainer newCell)
    {
        // Эта операция в данном виджете имеет собственную логику
        _TextField.setText(_TextField.getText() + " " + (String) newCell.getValue());
    }
}
