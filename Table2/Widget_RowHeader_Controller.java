package atom.components.Table2;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Добровольский
 */
public class Widget_RowHeader_Controller extends ATOMWidget_ExpandableStructure
{

    private Controller _Controller;
    private NestedSpacer _NestedSpacer;

    private JComponent _ReferenceWidget;

    public Widget_RowHeader_Controller()
    {
        _Controller = new Controller();
        _NestedSpacer = new NestedSpacer();

        setMainWidget(_Controller);
        setNestedWidget(_NestedSpacer);
    }

    public void setReferenceWidget(JComponent referenceWidget)
    {
        _ReferenceWidget = referenceWidget;
        _ReferenceWidget.addComponentListener(new Listener_OwnerStructureResize());
    }

    public void addMouseListener(MouseInputListener l)
    {
        _Controller.addMouseListener(l);
    }

    public void setHeight(int height)
    {
        _Controller.setHeight(height);
    }

    public void updateHeight()
    {
        int height = _ReferenceWidget.getSize().height;
        _NestedSpacer.setPreferredSize(new Dimension(0, height));
        revalidate();
    }

    private class Controller extends JComponent
    {

        private Spacer _Spacer;
        private Grip _Grip;

        public Controller()
        {
            setLayout(new MigLayout("flowy", "0[pref]0", "0[pref]0"));

            _Spacer = new Spacer();
            _Grip = new Grip();

            add(_Spacer, "grow");
            add(_Grip, "grow");

            setHeight(30);
        }

        public void addMouseListener(MouseInputListener l)
        {
            _Grip.addMouseListener(l);
            _Grip.addMouseMotionListener(l);
        }

        public void setHeight(int height)
        {
            height = height - 4;
            _Spacer.setPreferredSize(new Dimension(0, height));
            revalidate();
        }
    }

    private class Spacer extends JComponent
    {

        public Spacer()
        {
            setPreferredSize(new Dimension(30, 26));
            setVisible(false);
        }

        @Override
        protected void paintComponent(Graphics g)
        {
            g.setColor(Color.cyan);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    private class Grip extends JComponent
    {

        public Grip()
        {
            setPreferredSize(new Dimension(30, 5));
            setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
        }

//        @Override
//        protected void paintComponent(Graphics g)
//        {
//            g.setColor(Color.red);
//            g.fillRect(0, 0, getWidth(), getHeight());
//        }
    }

    private class NestedSpacer extends JComponent
    {

        public NestedSpacer()
        {
            setPreferredSize(new Dimension(30, 26));
        }

//        @Override
//        protected void paintComponent(Graphics g)
//        {
//            g.setColor(Color.cyan);
//            g.fillRect(0, 0, getWidth(), getHeight());
//        }
    }

    private class Listener_OwnerStructureResize extends ComponentAdapter
    {

        @Override
        public void componentResized(ComponentEvent e)
        {
            updateHeight();
        }
    }
}
