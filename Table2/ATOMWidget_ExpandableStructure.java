package atom.components.Table2;

import javax.swing.JComponent;
import net.miginfocom.swing.MigLayout;

/**
 *
 *
 * @author Доброволський
 */
public class ATOMWidget_ExpandableStructure extends JComponent
{

    protected JComponent _MainWidget;
    protected JComponent _NestedWidget;
    private boolean _isExpanded;

    public ATOMWidget_ExpandableStructure()
    {
        _isExpanded = false;
    }

    public void setMainWidget(JComponent mainComponent)
    {
        setLayout(new MigLayout("flowy", "0[pref]0", "0[pref]0"));

        _MainWidget = mainComponent;
        add(_MainWidget);
    }

    public JComponent getMainWidget()
    {
        return _MainWidget;
    }

    public void setNestedWidget(JComponent nestedWidget)
    {
        _NestedWidget = nestedWidget;
    }

    public JComponent getNestedWidget()
    {
        return _NestedWidget;
    }

    public void expand()
    {
        if (!_isExpanded && (_NestedWidget != null))
        {
            add(_NestedWidget);
            _NestedWidget.revalidate();
            _isExpanded = true;
        }
    }

    public void collapse()
    {
        if (_isExpanded)
        {
            remove(_NestedWidget);
            revalidate();
            _isExpanded = false;
        }
    }
}
