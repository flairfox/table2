package atom.components.Table2;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author Добровольский
 */
public class Widget_ColumnHeader_Controller extends JComponent
{

    private Spacer _Spacer;
    private Grip _Grip;

    public Widget_ColumnHeader_Controller()
    {
        _Spacer = new Spacer();
        _Spacer.setOpaque(true);
        
        _Grip = new Grip();
    }

    private class Spacer extends JComponent
    {

        public Spacer()
        {
            setPreferredSize(new Dimension(0, 30));
        }
        
    }

    private class Grip extends JComponent
    {

    }
}
