package atom.components.Table2;

import atom.data.TableModel2.ITable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JComponent;
import net.miginfocom.swing.MigLayout;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogic;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.ATOMEvent;
import atom.events2.ATOMEventDelegate;
import atom.events2.IATOMEvent;
import atom.events2.IATOMEventDelegate;
import atom.names.ATOMName;
import atom.names.IATOMName;
import java.util.Random;

/**
 *
 * @author Добровольский
 */
public class Widget_RowStructure extends JComponent implements IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    private Widget_RowListHeader _RowListHeader;
    private Widget_RowList _RowList;
    private LinkedList<Row> _Rows;

    private HashMap<Type, IRenderer_RowWidget> _RenderersRowWidget;
    private HashMap<Type, IRenderer_RowHeaderView> _RenderersRowHeader;
    private HashMap<Type, IRenderer_NestedWidget> _RenderersNestedWidget;
    private HashMap<Type, IRenderer_StructureMarkers> _RenderersStructureMarkers;

    private Listener_RowChangeState _Listener_RowChangeState;

    private Row _SelectedRow;

    private Random _Random;
    
    public Widget_RowStructure()
    {
        setLayout(new MigLayout("flowx", "0[pref]1[pref]0", "0[pref]0[pref]0"));
        
        _Random = new Random();

        _RowListHeader = new Widget_RowListHeader(this);
        _RowList = new Widget_RowList();

        _Rows = new LinkedList<Row>();

        _RenderersRowWidget = new LinkedHashMap<Type, IRenderer_RowWidget>();
        _RenderersRowHeader = new LinkedHashMap<Type, IRenderer_RowHeaderView>();
        _RenderersNestedWidget = new LinkedHashMap<Type, IRenderer_NestedWidget>();
        _RenderersStructureMarkers = new LinkedHashMap<Type, IRenderer_StructureMarkers>();

        _Listener_RowChangeState = new Listener_RowChangeState();

        add(_RowListHeader, "west, gapright 1px");
        add(_RowList);
    }

    protected Widget_RowStructure getOwnerStructure()
    {
        return this;
    }

    protected void selectRow(Row row)
    {
        if (_SelectedRow != null)
        {
            _SelectedRow.setSelected(false);
        }
        _SelectedRow = row;

        if (_SelectedRow != null)
        {
            _SelectedRow.setSelected(true);
        }
    }

    public int getPreferredWidth()
    {
        return _RowList.getPreferredSize().width;
    }

    public LinkedList<RowHeader> getRowHeaders()
    {
        return _RowListHeader.getRowHeaders();
    }

    private JComponent renderRowWidget(Row row)
    {
        // 1. Создаем виджет ряда, используя зарегистрированный пользователем виджет или, в отсутсвии такового, дефолтный виджет
        ITable.IRow modelRow = row.getModelRow();
        JComponent rowWidget;

        if ((modelRow.getUserObject() != null) && (_RenderersRowWidget.containsKey(modelRow.getUserObject().getClass())))
        {
            IRenderer_RowWidget rendererRowWidget = _RenderersRowWidget.get(modelRow.getUserObject().getClass());
            rowWidget = rendererRowWidget.createRowWidget(modelRow);
        } else if (_RenderersRowWidget.containsKey(modelRow.getClass()))
        {
            IRenderer_RowWidget rendererRowWidget = _RenderersRowWidget.get(modelRow.getClass());
            rowWidget = rendererRowWidget.createRowWidget(modelRow);
        } else
        {
            rowWidget = createDefaultRowWidget();
        }

        return rowWidget;
    }

    private Widget_RowHeader_View renderRowHeaderView(Row row)
    {
        // 2. Создаем виджет заголовка ряда, используя зарегистрированный пользователем виджет или, в отсутсвии такового, дефолтный виджет
        ITable.IRow modelRow = row.getModelRow();
        Widget_RowHeader_View rowHeaderView;

        if ((modelRow.getUserObject() != null) && (_RenderersRowHeader.containsKey(modelRow.getUserObject().getClass())))
        {
            IRenderer_RowHeaderView rendererRowHeader = _RenderersRowHeader.get(modelRow.getUserObject().getClass());
            rowHeaderView = rendererRowHeader.createRowHeaderView();

        } else if (_RenderersRowHeader.containsKey(modelRow.getClass()))
        {
            IRenderer_RowHeaderView rendererRowHeader = _RenderersRowHeader.get(modelRow.getClass());
            rowHeaderView = rendererRowHeader.createRowHeaderView();
        } else
        {
            rowHeaderView = createDefaultRowHeader();
        }

        rowHeaderView.addMouseListener(_Listener_RowChangeState);

        rowHeaderView.setOwnerRow(row);

        return rowHeaderView;
    }

    private JComponent renderNesterWidget(Row row)
    {
        // 3. Создаем виджет вложенного компонента ряда, используя зарегистрированный пользователем виджет или, в отсутсвии такового, дефолтный виджет
        ITable.IRow modelRow = row.getModelRow();
        JComponent nestedWidget;

        if ((modelRow.getUserObject() != null) && (_RenderersNestedWidget.containsKey(modelRow.getUserObject().getClass())))
        {
            IRenderer_NestedWidget rendererNestedWidget = _RenderersNestedWidget.get(modelRow.getUserObject().getClass());
            nestedWidget = rendererNestedWidget.createNestedWidget();
        } else if (_RenderersNestedWidget.containsKey(modelRow.getClass()))
        {
            IRenderer_NestedWidget rendererNestedWidget = _RenderersNestedWidget.get(modelRow.getClass());
            nestedWidget = rendererNestedWidget.createNestedWidget();
        } else
        {
            nestedWidget = createDefaultNestedWidget(row);
        }

        return nestedWidget;
    }

    public JComponent renderStructureMarkers(Row row)
    {
        return createDefaultStructureMarkers();
    }

    public Row insertRow(int index, ITable.IRow modelRow)
    {
        Row newRow = new Row(this);

        newRow.setModelRow(modelRow);
        newRow.setRowWidget(renderRowWidget(newRow));
        newRow.setRowHeader(renderRowHeaderView(newRow));
        
//        if(_Random.nextBoolean())
//        {
//            newRow.setExpandable(true);
//        }

        if (modelRow.getNestedTable() != null)
        {
            newRow.setExpandable(true);
        }

        _Rows.add(newRow);

        _RowList.insertRow(index, newRow);
        _RowListHeader.insertRowHeader(index, newRow);

        _RowListHeader.enumerateRows();

        return newRow;
    }

    public Row getRow(int index)
    {
        return _Rows.get(index);
    }

    public void removeRow(int index)
    {
        _RowList.remove(index);
        _Rows.remove(index);
    }

    public LinkedList<Row> getRows()
    {
        return _Rows;
    }

    public int getRowCount()
    {
        return _Rows.size();
    }

    // Блок регистрации новых рендереров
    public void registerRowWidgetRenderer(Type key, IRenderer_RowWidget newRenderer)
    {
        _RenderersRowWidget.put(key, newRenderer);
    }

    public void registerRowHeaderRenderer(Type key, IRenderer_RowHeaderView newRenderer)
    {
        _RenderersRowHeader.put(key, newRenderer);
    }

    public void registerNestedWidgetRenderer(Type key, IRenderer_NestedWidget newRenderer)
    {
        _RenderersNestedWidget.put(key, newRenderer);
    }

    public void registerStructureMarkersRenderer(Type key, IRenderer_StructureMarkers newRenderer)
    {
        _RenderersStructureMarkers.put(key, newRenderer);
    }

    // Блок реализации дефолтных рендереров
    private JComponent createDefaultRowWidget()
    {
        return new Widget_RowWidget_Cells();
    }

    private Widget_RowHeader_View createDefaultRowHeader()
    {
        return new Widget_RowHeader_View();
    }

    public JComponent createDefaultNestedWidget(Row row)
    {
        return new Widget_RowStructure();
    }

    public JComponent createDefaultStructureMarkers()
    {
        Widget_StructureMarkers_Table structureMarkers = new Widget_StructureMarkers_Table();

        return structureMarkers;
    }

    protected class Listener_RowChangeState extends MouseAdapter
    {

        private boolean isAlreadyOneClick;

        @Override
        public void mousePressed(MouseEvent e)
        {
            Row row = ((Widget_RowHeader_View) e.getSource()).getOwnerRow();
            Events.RowSelected.Rise(getOwnerStructure(), row);
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            Row row = ((Widget_RowHeader_View) e.getSource()).getOwnerRow();

            if (row.isExpandable())
            {

                if (isAlreadyOneClick)
                {
                    if (row.getNestedWidget() == null)
                    {
                        JComponent nestedComponent = renderNesterWidget(row);
                        row.setNestedWidget(nestedComponent);

                        Widget_StructureMarkers_Table structureMarkers = (Widget_StructureMarkers_Table) renderStructureMarkers(row);
                        structureMarkers.setNestedStructure(nestedComponent);

                        row.setStructureMarkers(structureMarkers);
                    }

                    row.changeState();
                    isAlreadyOneClick = false;
                } else
                {
                    isAlreadyOneClick = true;
                    Timer t = new Timer("doubleclickTimer", false);
                    t.schedule(new TimerTask()
                    {

                        @Override
                        public void run()
                        {
                            isAlreadyOneClick = false;
                        }
                    }, 300);
                }
            }
        }
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    /**
     * Делегат событий класса
     */
    //protected IATOMEventDelegate _Delegate = new ATOMEventDelegate(); // для классов без собственной логики событий
    protected IATOMEventDelegate _Delegate = new EventLogic(); // Для класса с собственной логикой событий

    @Override
    public IATOMEventDelegate Delegate()
    {
        return _Delegate;              //Альтернатива 1. Класс ведет собственный учет слушателей для генерируемых им событий
        //                и/или имеет логику обработки собственных генерируемых событий
        //return _OwnerManager.Delegate();  //Альтернатива 2. Класс полностью перенаправляет учет, генерацию событий и обработку логики в управляющий класс
        // Используется, когда класс может генерировать события, но в программе создается большое число
        // экземпляров классов  при редких индивидуальных подписчиках. Позволяет иключить необходимость
        // подписываться управляющему классу к каждому подчиненному экземпляру подчиненного класса.
        // Пример 1. Класс "Ячейка в таблице" может перенаправлять учет и обработку событий в
        // класс "Строка таблицы", которая в свою очередь может перенаправлять управление в
        // класс "Таблица".
        // Пример 2. Класс "Атрибут сущности данных" может перенаправлять учет и обработку событий
        // в класс "Сущность данных".

    }

    /*Логика обработки событий атрибута*/
    @Override
    public IATOMEventLogic EventLogic()
    {
        return _Delegate;
    }

    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ATOMEvent
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super(eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 1
        public static class RowSelected extends Events
        {

            /**
             * Тип события "Выделение ряда"
             */
            protected final static IATOMName _RowSelected = new ATOMName("EventSource.Events.RowSelected");

            //Параметры события
            //int parametr1;
            public Row SelectedRow;

            //int parametr2;
            public RowSelected(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public RowSelected(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _RowSelected;
            }

            protected static RowSelected Create(IATOMEventDelegateOwner eventSource, Object eventTarget, Row selectedRow)
            {
                RowSelected e = new RowSelected(eventSource, eventTarget);
                e.SelectedRow = selectedRow;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Выделение ряда* @param
             * eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Widget_RowStructure eventSource, Row selectedRow)
            {
                RowSelected e = Create(eventSource, eventSource, selectedRow);

                return eventSource.Delegate().Rise(e);
            }

        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 2
    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //===============================//##################################################################
    //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Выделение ряда" #####">

    /**
     * Класс экземпляра события "Выделение ряда"
     */
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Выделение ряда""
//##################################################################
    //========================================================
    //</editor-fold>
    //##################################################################
    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMEventDelegate        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //    isAllowEvent = OnBefore_Event1(event);
            // }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.RowSelected.class.isAssignableFrom(eventClass))
            {
                Events.RowSelected event = (Events.RowSelected) e;
                isDone = OnDo_RowSelected(event);
            }
            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

    private boolean OnDo_RowSelected(Events.RowSelected event)
    {
        selectRow(event.SelectedRow);
        return true;
    }
//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
