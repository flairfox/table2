package atom.components.Table2;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 * Заголовочный элемент ряда, отображает номер ряда и управляет раскрытием ряда,
 * если у ряда есть вложенный компонент
 *
 * @author Добровольский
 */
public class Widget_RowHeader_View extends JButton
{

    private Row _OwnerRow;
    private Color _BackgroundColor;
    private boolean _isExpandable, _isExpanded;

    public Widget_RowHeader_View()
    {
        _isExpandable = false;
        _isExpanded = false;

        _BackgroundColor = new Color(180, 217, 241);

        Font font = getFont();
        font.deriveFont(Font.BOLD | 12);
        setFont(font);
        setBorder(BorderFactory.createEmptyBorder());
        setFocusPainted(false);
        setContentAreaFilled(false);
        setBackground(Color.LIGHT_GRAY);
        setPreferredSize(new Dimension(30, 30));
    }

    @Override
    public void setSelected(boolean flag)
    {
        if (flag)
        {
            _BackgroundColor = new Color(30, 119, 162);
        } else
        {
            _BackgroundColor = new Color(180, 217, 241);
        }

        repaint();
    }

    public void setRowNumber(int number)
    {
        setText(Integer.toString(number));
    }

    public void setExpandable(boolean flag)
    {
        _isExpandable = flag;
        repaint();
    }

    public void changeState()
    {
        _isExpanded = !_isExpanded;
        repaint();
    }

    private void drawExpandMark(Graphics2D g2)
    {
        g2.setColor(Color.WHITE);
        g2.fillRect(0, this.getHeight() - 9, 8, 8);

        g2.setColor(Color.DARK_GRAY);
        g2.drawRect(0, this.getHeight() - 9, 8, 8);
        g2.drawLine(2, this.getHeight() - 5, 6, this.getHeight() - 5);
        g2.drawLine(4, this.getHeight() - 7, 4, this.getHeight() - 3);
    }

    private void drawExpandedMark(Graphics2D g2)
    {
        g2.setColor(Color.WHITE);
        g2.fillRect(0, this.getHeight() - 9, 8, 8);

        g2.setColor(Color.DARK_GRAY);

        g2.drawRect(0, this.getHeight() - 9, 8, 8);
        g2.drawLine(2, this.getHeight() - 5, 6, this.getHeight() - 5);
    }

    public void setOwnerRow(Row ownerRow)
    {
        _OwnerRow = ownerRow;
    }

    public Row getOwnerRow()
    {
        return _OwnerRow;
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(_BackgroundColor);
        g2.fillRect(0, 0, getWidth(), getHeight());

        g2.setColor(Color.BLACK);

        super.paintComponent(g);

        if (_isExpandable)
        {
            setCursor(new Cursor(Cursor.HAND_CURSOR));

            if (!_isExpanded)
            {
                drawExpandMark(g2);
            } else
            {
                drawExpandedMark(g2);
            }
        }
    }
}
