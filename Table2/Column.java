package atom.components.Table2;

import atom.data.DataTypeInfo;
import atom.data.TableModel2.ITable;
import atom.data.TableModel2.ITableView;
import java.lang.reflect.Type;
import java.util.HashMap;

/**
 * Столбец таблицы
 *
 * @author Добровольский
 */
public class Column
{
    private Widget_TableWrap _OwnerTable;
    
    private int _Width;
    
    private int _MinimumWidth;
    
    private String _Caption;
    
    private String _AttributeName;
    
    //private ColumnHeader _ColumnHeader;

    /**
     * Столбец из модели данных
     */
    private ITable.IColumn _ModelColumn;

    /**
     * Временное поле рендерера ячеек этого столбца
     */
    private IRenderer_Cell _CellRenderer;

    /**
     * Имя столбца
     */
    private String _Name;

    /**
     * Роль столбца
     */
    private String _Role;

    /**
     * Тип данных в столбце
     */
    private DataTypeInfo.ELogicTypes _Type;

    /**
     * Рендереры для ячеек
     */
    private HashMap<Type, IRenderer_Cell> _CellWidgetRenderers;

    /**
     * Инициализация столбца
     *
     * @param ownerTable
     * @param modelColumn Столбец из модели данных
     */
    public Column(Widget_TableWrap ownerTable, ITable.IColumn modelColumn)
    {
        _ModelColumn = modelColumn;
        _OwnerTable = ownerTable;

        _Name = modelColumn.getName();
        _Role = modelColumn.getRole();
        _Type = modelColumn.getType();
        
        _Width = 150;
        _MinimumWidth = 0;
    }
    
    public Column(Widget_TableWrap ownerTable, ITableView.IColumnView modelColumnView)
    {
        _ModelColumn = modelColumnView;
        _OwnerTable = ownerTable;

        _Name = modelColumnView.getName();
        _Role = modelColumnView.getRole();
        _Type = modelColumnView.getType();
        
        _Width = 150;
        _MinimumWidth = 0;
    }
    
    public void setCaption(String columnCaption)
    {
        _Caption = columnCaption;
    }
    
    public String getCaption()
    {
        return _Caption; 
    }
    
    public void setAttributeName(String attributeName)
    {
        _AttributeName = attributeName;
    }
    
    public String getAttributeName()
    {
        return _AttributeName;
    }

    /**
     * Установка рендерера для ячеек столбца
     *
     * @param cellRenderer Рендерер ячейки
     */
    public void setCellRenderer(IRenderer_Cell cellRenderer)
    {
        _CellRenderer = cellRenderer;
    }

    /**
     * Получение рендерера для ячеек столбца
     *
     * @return Рендерер ячейки
     */
    public IRenderer_Cell getCellRenderer()
    {
        if (_CellRenderer != null)
        {
            return _CellRenderer;
        } else
        {
            return createDefalutCell();
        }
    }

    public String getName()
    {
        return _Name;
    }

    public String getRole()
    {
        return _Role;
    }

    public IRenderer_Cell createDefalutCell()
    {
        return new Renderer_CellText();
    }
    
    public void setWidth(int width)
    {
        if (_Width >= _MinimumWidth)
        {
            _Width = width;
            _OwnerTable.updateColumnWidth(this);
        }
    }
    
    public void setMinimumWidth(int width)
    {
        _MinimumWidth = width;
    }
    
    public int getWidth()
    {
        return _Width;
    }
    
    public int getMinimumWidth()
    {
        return _MinimumWidth;
    }
    
    public int getIndex()
    {
        return _OwnerTable.getColumns().indexOf(this);
    }
}
