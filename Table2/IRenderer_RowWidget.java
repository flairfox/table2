package atom.components.Table2;

import atom.data.TableModel2.ITable;
import javax.swing.JComponent;

/**
 * Интерфейс для подключения рендереров строк
 *
 * @author Добровольский
 */
public interface IRenderer_RowWidget
{
    /**
     * Создание элемента отображения данных строки
     * @param modelRow Строка из модели данных
     * @return Элемент отображения
     */
    public JComponent createRowWidget(ITable.IRow modelRow);
}
