package atom.components.Table2;

/**
 *
 * @author Добровольский
 */
public interface IRenderer_RowHeaderView
{
    public Widget_RowHeader_View createRowHeaderView();
}
