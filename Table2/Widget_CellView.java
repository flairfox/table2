package atom.components.Table2;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import net.miginfocom.swing.MigLayout;

/**
 * Виджет отображения ячейки
 *
 * @author Добровольский
 */
public abstract class Widget_CellView extends JComponent
{

    protected Color _BackgroundColor;
    protected Color _TextColor;
    protected Font _TextFont;
    protected int _TextSize;

    public Widget_CellView()
    {
        setLayout(new MigLayout("fill", "0[pref]0", "0[pref]0"));
    }

    public void setBackgroundColor(Color color)
    {
        _BackgroundColor = color;
    }

    public Color getBackgroundColor()
    {
        return _BackgroundColor;
    }

    public void setTextColor(Color color)
    {
        _TextColor = color;
    }

    public Color getTextColor()
    {
        return _TextColor;
    }

    public void setTextFont(Font font)
    {
        _TextFont = font;
    }

    public Font getTextFont()
    {
        return _TextFont;
    }

    public void setTextSize(int size)
    {
        _TextSize = size;
    }

    public int getTextSize()
    {
        return _TextSize;
    }

    public abstract int getMinimumWidth();

    public abstract int getDefaulWidth();

    public abstract void updateValue(Object value);

    @Override
    public abstract void addMouseListener(MouseListener l);
}
