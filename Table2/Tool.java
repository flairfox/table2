package atom.components.Table2;

import javax.swing.JComponent;

/**
 *
 * @author Добровольский
 */
public abstract class Tool extends JComponent
{
    public abstract void performAction(Object operableObject);
}
