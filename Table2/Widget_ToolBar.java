package atom.components.Table2;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.JComponent;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Добровольский
 */
public class Widget_ToolBar extends JComponent
{
    private LinkedList<Tool> _Tools;

    public Widget_ToolBar()
    {
        setLayout(new MigLayout("flowx, ay center", "0[]1[]0", "0[]0[]0"));

        setPreferredSize(new Dimension(150, 30));
        setMinimumSize(new Dimension(0, 30));
    }
    
    public void addTool(JComponent tool)
    {
        add(tool);
    }
    
    public void removeTool(JComponent tool)
    {
        remove(tool);
    }
    
    private class Listener_ActionPerform implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            //((Tool) e.getSource()).performAction(TOP_ALIGNMENT);
        }
        
    }
}
