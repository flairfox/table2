package atom.components.Table2;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Box;

/**
 * Класс вложенной таблицы
 *
 * @author Добровольский
 */
public class Widget_TableNested extends Widget_TableWrap
{

    private Widget_TableWrap _OwnerTable;
    
    @Override
    protected void selectTableCell(Widget_CellContainer cell)
    {
        _OwnerTable.selectTableCell(cell);
    }
    
    @Override
    protected void selectTableRow(Row row)
    {
        _OwnerTable.selectTableRow(row);
        
        for(Column column : _Columns)
        {
            updateColumnWidth(column);
        }
        
    }

    public Widget_TableNested()
    {
        super();
        _LayerView.add(Box.createRigidArea(new Dimension(0, 10)), "north");
        _LayerView.add(Box.createRigidArea(new Dimension(0, 10)), "south");
    }

    public void setOwnerTable(Widget_TableWrap ownerTable)
    {
        _OwnerTable = ownerTable;
    }
    
    @Override
    public void startEditMode(Widget_CellContainer cell, Widget_CellEdit cellEdit)
    {
        _OwnerTable.startEditMode(cell, cellEdit);
    }

    @Override
    protected Widget_TableWrap getOwnerTable()
    {
        return _OwnerTable.getOwnerTable();
    }

    @Override
    protected void paintComponent(Graphics g)
    {
    }
}
