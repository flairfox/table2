package atom.data.TableModel2;

import atom.data.DataTypeInfo;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author Добровольский
 */
public class Model_TableView extends Abstract_Table<Model_TableView, Model_RowView, Model_ColumnView, Model_CellView> 
                             implements ITableView<Model_TableView, Model_RowView, Model_ColumnView, Model_CellView>, 
                                        IFilter<Model_Row>, ISorter<LinkedList<Model_Row>>
{

    private ITable _DataModel;
    private LinkedList<IFilterLogic> _FilterLogics = new LinkedList<IFilterLogic>();
    private LinkedList<ISortLogic> _SortLogics = new LinkedList<ISortLogic>();
    
    private HashMap<Type, ITableViewStructure> _TableViewSetup = new HashMap<Type, ITableViewStructure>();

    @Override
    public Model_RowView createDefaultRowInstance()
    {
        return new Model_RowView();
    }

    @Override
    public Model_ColumnView createDefaultColumnInstance()
    {
        return new Model_ColumnView(this, "", "", DataTypeInfo.ELogicTypes.String);
    }

    @Override
    public Model_CellView createDefaultCellInstance(Model_RowView ownerRow)
    {
        Model_CellView newCell = new Model_CellView();
        newCell.setOwnerRow(ownerRow);
        return newCell;
    }

    @Override
    public void insertRow(int index, IRow row)
    {

    }

    @SuppressWarnings("unchecked")    
    @Override
    public void insertColumn(int index, String attributeName, String columnCaption)
    {
        Model_Column dataColumn = findDataColumn(attributeName);
        int dataColumnIndex = _DataModel.getColumnIndex(dataColumn);

        Model_ColumnView newColumnView;

        if (dataColumn != null)
        {
            newColumnView = createDefaultColumnInstance();

            newColumnView.setAttributeName(attributeName);
            newColumnView.setCaption(columnCaption);

            newColumnView.setName(dataColumn.getName());
            newColumnView.setRole(dataColumn.getRole());

            _Columns.add(index, newColumnView);

            for (Model_RowView rowView : _Rows)
            {
                Model_CellView newCell = createDefaultCellInstance(rowView);
                Model_Cell dataCell = (Model_Cell) rowView.getDataRow().getCell(dataColumnIndex);

                newCell.setValue(dataCell.getValue());

                rowView.addCell(newCell);
            }
        }
    }

    @SuppressWarnings("unchecked")    
    @Override
    public void insertCalculatedColumn(int index, ICalculator calculator, String columnCaption)
    {
        Model_ColumnView newColumnView;

        newColumnView = createDefaultColumnInstance();
        newColumnView.setCaption(columnCaption);

        _Columns.add(index, newColumnView);

        for (Model_RowView rowView : _Rows)
        {
            Model_CellView newCell = createDefaultCellInstance(rowView);
            newCell.setValue(calculator.calculateValue(rowView.getDataRow()));
            rowView.addCell(newCell);
        }
    }

    @SuppressWarnings("unchecked")    
    @Override
    public void setDataModel(ITable dataModel)
    {
        _DataModel = dataModel;
        _Rows.clear();
        
        LinkedList<Model_Row> sortedRows = sortAscending(_DataModel.getRows());
        //LinkedList<Model_Row> sortedRows = sortDescending(_DataModel.getRows());
        
        for (Model_Row dataRow : sortedRows)
        {
            if (isFiltered(dataRow))
            {
                Model_RowView newRowView = createDefaultRowInstance();
                newRowView.setUserObject(dataRow.getUserObject());
                newRowView.setDataRow(dataRow);

                _Rows.add(newRowView);
            }
        }
    }

    @SuppressWarnings("unchecked")    
    public Model_Column findDataColumn(String attributeName)
    {
        if (_DataModel == null)
        {
            return null;
        }

        for (Model_Column dataColumn : (LinkedList<Model_Column>) _DataModel.getColumns())
        {
            if (dataColumn.getAttributeName().equals(attributeName))
            {
                return dataColumn;
            }
        }

        return null;
    }

    @Override
    public void addFilterLogic(IFilterLogic filterLogic)
    {
        _FilterLogics.add(filterLogic);
    }

    @Override
    public void removeFilterLogic(IFilterLogic filterLogic)
    {
        _FilterLogics.remove(filterLogic);
    }

    @SuppressWarnings("unchecked")    
    @Override
    public boolean isFiltered(Model_Row row)
    {
        for (IFilterLogic filterLogic : _FilterLogics)
        {
            if (filterLogic.isFiltered(row))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public void addSortLogic(ISortLogic sortLogic)
    {
        _SortLogics.add(sortLogic);
    }

    @Override
    public void removeSortLogic(ISortLogic sortLogic)
    {
        _SortLogics.remove(sortLogic);
    }

    @SuppressWarnings("unchecked")    
    @Override
    public LinkedList<Model_Row> sortAscending(LinkedList<Model_Row> collection)
    {
        LinkedList<Model_Row> sortedCollection = new LinkedList<Model_Row>(collection);

        Collections.sort(sortedCollection, new Comparator<Model_Row>()
        {
            @Override
            public int compare(Model_Row item1, Model_Row item2)
            {
                for(ISortLogic sortLogic : _SortLogics)
                {
                    int c = sortLogic.compare(item1, item2);
                    if(c != 0)
                    {
                        return c;
                    }
                }
                
                return 0;
            }
        });
        
        return sortedCollection;
    }

    @SuppressWarnings("unchecked")    
    @Override
    public LinkedList<Model_Row> sortDescending(LinkedList<Model_Row> collection)
    {
        LinkedList<Model_Row> sortedCollection = new LinkedList<Model_Row>(collection);

        Collections.sort(sortedCollection, new Comparator<Model_Row>()
        {
            @Override
            public int compare(Model_Row item1, Model_Row item2)
            {
                for (ISortLogic sortLogic : _SortLogics)
                {
                    int c = sortLogic.compare(item1, item2);
                    if (c != 0)
                    {
                        return -c;
                    }
                }

                return 0;
            }
        });

        return sortedCollection;
    }

    @Override
    public void RegisterNestedTableViewStructure(Type userObjectType, ITableViewStructure tableViewSetup)
    {
        _TableViewSetup.put(userObjectType, tableViewSetup);
    }

    @Override
    public ITableView CreateTableViewStructure(ITable.IRow expandableRow)
    { 
        return _TableViewSetup.get(expandableRow.getUserObject().getClass()).createTableViewSetup(expandableRow);
    }
    
    public ITableView createDefaultTableViewSetup()
    {
        
        
        return null;
    }
}
