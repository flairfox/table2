package atom.data.TableModel2;

import atom.data.DataTypeInfo;
import atom.data.entities.DataEntityAttributeDescription;

/**
 *
 * @author Добровольский
 */
public class Model_ColumnView extends Abstract_Column<Model_TableView, Model_RowView, Model_ColumnView, Model_CellView> implements ITableView.IColumnView<Model_TableView, Model_RowView, Model_ColumnView, Model_CellView>
{
    String _Caption;
    DataEntityAttributeDescription _Description;

    public Model_ColumnView(Model_TableView ownerTable, String name, String role, DataTypeInfo.ELogicTypes type)
    {
        super(ownerTable, name, role, type);
    }
    
    public void setCaption(String columnCaption)
    {
        _Caption = columnCaption;
    }
    
    @Override
    public String getCaption()
    {
        return _Caption;
    }

    @Override
    protected Model_CellView createCellInstance(Model_RowView ownerRow)
    {
        Model_CellView newCell = new Model_CellView(); 
        newCell.setOwnerRow(ownerRow);
        return newCell;
    }
}
