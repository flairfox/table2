/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atom.data.TableModel2;

import atom.data.DataTypeInfo;

/**
 *
 * @author Vadim
 */
public class Model_Column extends Abstract_Column<Model_Table, Model_Row, Model_Column, Model_Cell> implements ITable.IColumn<Model_Table, Model_Row, Model_Column, Model_Cell>
{
    
    public Model_Column(Model_Table ownerTable, String name, String role, DataTypeInfo.ELogicTypes type)
    {
        super(ownerTable, name, role, type);
    }

    @Override
    protected Model_Cell createCellInstance(Model_Row ownerRow)
    {
        Model_Cell newCell = new Model_Cell();
        newCell.setOwnerRow(ownerRow);
        return newCell;
    }
    
}
