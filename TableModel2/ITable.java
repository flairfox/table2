package atom.data.TableModel2;

import atom.data.DataTypeInfo;
import atom.events2.ATOMEvent;
import atom.events2.ATOMEventDelegate;
import atom.events2.IATOMEventDelegate;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogic;
import atom.names.ATOMName;
import atom.names.IATOMName;
import java.util.LinkedList;

/**
 * Базовый интерфейс таблицы
 *
 * @author Добровольский
 * @param <TRow>
 * @param <TColumn>
 */
public interface ITable<TTable extends ITable<TTable, TRow, TColumn, TCell>,
                        TRow extends ITable.IRow<TTable, TRow, TColumn, TCell>,
                        TColumn extends ITable.IColumn<TTable, TRow, TColumn, TCell>,
                        TCell extends ITable.ICell<TTable, TRow, TColumn, TCell>>
{
    public TColumn createDefaultColumnInstance();
    
    public TRow createDefaultRowInstance();
    
    public TCell createDefaultCellInstance(TRow ownerRow);

    /**
     * Добавить пустую строку и вернуть ссылку на неё для наполнения
     *
     * @return Строка
     */
    public TRow addRow();

    /**
     * Добавить пустую строку на определенный индекс и вернуть ссылку на неё для
     * наполнения
     *
     * @param index Индекс
     * @return Строка
     */
    public TRow addRow(int index);

    /**
     * Добавить строку
     *
     * @param newRow Новая строка
     */
    public void addRow(TRow newRow);

    /**
     * Добавить строку
     *
     * @param index Индекс
     * @param newRow Новая строка
     */
    public void addRow(int index, TRow newRow);

    /**
     * Поиск строки по индексу
     *
     * @param index Индекс
     * @return Строка
     */
    public TRow getRow(int index);

    /**
     * Получение коллекции строк
     *
     * @return Коллекция строк
     */
    public LinkedList<TRow> getRows();

    /**
     * Получение количества строк
     *
     * @return Количество строк
     */
    public int getRowCount();

    /**
     * Удаление строки по ее индексу
     *
     * @param index Индекс
     */
    public void removeRow(int index);

    /**
     * Создание и добавление столбца
     *
     * @param name Имя столбца
     * @param role Роль столбца
     * @param dataType Тип данных
     */
    public void addColumn(String name, String role, DataTypeInfo.ELogicTypes dataType);

    /**
     * Создание и добавление столбца
     *
     * @param name Имя столбца
     * @param dataType Тип данных
     */
    public void addColumn(String name, DataTypeInfo.ELogicTypes dataType);

    /**
     * Добавление столбца
     *
     * @param newColumn Новый столбец
     */
    public void addColumn(TColumn newColumn);

    /**
     * Поиск столбца по индексу
     *
     * @param index Индекс
     * @return Столбец
     */
    public TColumn getColumn(int index);
    
    public int getColumnIndex(TColumn column);

    /**
     * Поиск столбца таблицы по имени и роли
     *
     * @param name Имя столбца
     * @param role Роль столбца
     * @return Столбец
     */
    public TColumn getColumn(String name, String role);

    /**
     * Получение коллекции столбцов
     *
     * @return Коллекция столбцов
     */
    public LinkedList<TColumn> getColumns();

    /**
     * Получение количества столбцов
     *
     * @return Количество столбцов
     */
    public int getColumnCount();

    /**
     * Удаление столбца по его индексу
     *
     * @param index Индекс
     */
    public void removeColumn(int index);

    /**
     * Удаление столбца по его имени и роли
     *
     * @param name Имя столбца
     * @param role Роль столбца
     */
    public void removeColumn(String name, String role);

    /**
     * Поиск ячейки по индексам строки и столбца
     *
     * @param rowIndex Индекс строки
     * @param columnIndex Индекс столбца
     * @return Ячейка
     */
    public TCell getCell(int rowIndex, int columnIndex);

    /**
     * Получение индекса ряда
     *
     * @param row Ряд
     * @return Индекс
     */
    public int getRowIndex(TRow row);

    public static class Events extends ATOMEvent
    {

        /**
         * Таблица, в которой происходит событие
         */
        public ITable Table;

        public Events(Object eventSource)
        {
            super(eventSource, null);
        }

        public Events(Object eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }
    }

    public static class EventLogic extends ATOMEventDelegate
    {
    }

    public IATOMEventDelegate Delegate();

    public IATOMEventLogic EventLogic();

    /**
     * Базовый интерфейс столбца
     */
    public interface IColumn<TTable extends ITable<TTable, TRow, TColumn, TCell>, 
                             TRow extends IRow<TTable, TRow, TColumn, TCell>, 
                             TColumn extends IColumn<TTable, TRow, TColumn, TCell>, 
                             TCell extends ICell<TTable, TRow, TColumn, TCell>>
    {

        public String getAttributeName();
        
        public void setAttributeName(String attributeName);

        /**
         * Установка имени столбца
         *
         * @param name Имя столбца
         */
        public void setName(String name);
        
        public void setUserObject(Object userObject);
        public Object getUserObject();

        /**
         * Получение имени столбца
         *
         * @return Имя столбца
         */
        public String getName();
        
        /**
         * Установка роли столбца
         *
         * @param role Роль столбца
         */
        public void setRole(String role);

        /**
         * Получение роли столбца
         *
         * @return Роль столбца
         */
        public String getRole();

        /**
         * Установка типа данных
         *
         * @param type Тип данных
         */
        public void setType(DataTypeInfo.ELogicTypes type);

        /**
         * Получение типа данных
         *
         * @return Тип данных
         */
        public DataTypeInfo.ELogicTypes getType();

        public void setOwnerTable(TTable ownerTable);
        
        /**
         * Получение таблицы-владельца
         *
         * @return Таблица-владелец
         */
        public TTable getOwnerTable();

        public TCell getCell(int index);
    }

    /**
     * Базовый интерфейс строки
     */
    public interface IRow<TTable extends ITable<TTable, TRow, TColumn, TCell>, 
                          TRow extends IRow<TTable, TRow, TColumn, TCell>, 
                          TColumn extends IColumn<TTable, TRow, TColumn, TCell>, 
                          TCell extends ICell<TTable, TRow, TColumn, TCell>>
    {
        
        public TTable getNestedTable();
        
        public void setNestedTable(TTable nestedTable);
        
        /**
         * Получение количества ячеек в строке
         *
         * @return Количество ячеек
         */
        public int getCellCount();

        /**
         * Добавление ячейки по определенному индексу
         *
         * @param index Индекс
         * @param newCell Новая ячейка
         */
        public void addCell(int index, TCell newCell);

        /**
         * Добавление новой ячейки
         *
         * @param newCell Новая ячейка
         */
        public void addCell(TCell newCell);

        /**
         * Создание пустой ячейки и получение ссылки на нее для редактирования
         *
         * @return Ячейка
         */
        public TCell addCell();

        /**
         * Получение ячейки по индексу
         *
         * @param index Индекс
         * @return Ячейка
         */
        public TCell getCell(int index);
        
        public TCell getCell(String attributeName);

        /**
         * Установка пользовательских данных
         *
         * @param userData Пользовательские данные
         */
        public void setUserObject(Object userData);

        /**
         * Получение ползовательских данных
         *
         * @return Пользовательские данные
         */
        public Object getUserObject();

        public void setOwnerTable(TTable ownerTable);
        /**
         * Получение таблицы-владельца
         *
         * @return Таблица-владелец
         */
        public TTable getOwnerTable();

        /**
         * Установка типа ряда
         *
         * @param type Тип ряда
         */
        public void setType(String type);

        /**
         * Получение типа ряда
         *
         * @return Тип ряда
         */
        public String getType();

        /**
         * Получение индекс ячейки
         *
         * @param cell Ячейка
         * @return Индекс
         */
        public int getCellIndex(TCell cell);

        public LinkedList<TCell> getCells();

        public static class Events extends ATOMEvent
        {

            /**
             * Строка, в которой происходит событие
             */
            public IRow Row;

            public Events(Object eventSource)
            {
                super(eventSource, null);
            }

            public Events(Object eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }
        }

        public static class EventLogic extends ATOMEventDelegate
        {
        }

        public IATOMEventDelegate Delegate();

        public IATOMEventLogic EventLogic();

    }

    /**
     * Базовый интерфейс ячейки
     */
    public interface ICell<TTable extends ITable<TTable, TRow, TColumn, TCell>, 
                           TRow extends IRow<TTable, TRow, TColumn, TCell>, 
                           TColumn extends IColumn<TTable, TRow, TColumn, TCell>, 
                           TCell extends ICell<TTable, TRow, TColumn, TCell>>
    {

        /**
         * Установка значения данных в ячейку
         *
         * @param value Значение данных
         */
        public void setValue(Object value);

        /**
         * Получение значения данных из ячейки
         *
         * @return Значение данных
         */
        public Object getValue();

        public void setOwnerRow(TRow ownerRow);
        
        /**
         * Получение ряда-владельца
         *
         * @return Ряд-владелец
         */
        public TRow getOwnerRow();

        /**
         * Получение индекса строки-владельца
         *
         * @return Индекс строки
         */
        public int getRowIndex();

        /**
         * Получение индекса столбца
         *
         * @return Индекс столбца
         */
        public int getColumnIndex();

        public static class Events extends ATOMEvent
        {

            /**
             * Ячейка, в которой происходит событие
             */
            public ICell Cell;

            public Events(Object eventSource)
            {
                super(eventSource, null);
            }

            public Events(Object eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            public static class ValueChange extends Events
            {

                /**
                 * Тип события "Описание события"
                 */
                protected final static IATOMName _ValueChange = new ATOMName("Model_Cell.Events.ValueChange");

                /*  Новое значение ячейки    */
                public Object newValue;

                /*  Предыдущее значение ячейки*/
                public Object prevValue;

                public ValueChange(IATOMEventDelegateOwner eventSource)
                {
                    super(eventSource);
                }

                public ValueChange(IATOMEventDelegateOwner eventSource, Object eventTarget)
                {
                    super(eventSource, eventTarget);
                }

                @Override
                public IATOMName getEventName()
                {
                    return _ValueChange;
                }

                protected static ValueChange Create(IATOMEventDelegateOwner eventSource, Object eventTarget, Object prevValue, Object newValue)
                {
                    ValueChange e = new ValueChange(eventSource, eventTarget);
                    e.prevValue = prevValue;
                    e.newValue = newValue;

                    if (eventSource instanceof ITable.ICell)
                    {
                        e.Cell = (ITable.ICell) eventSource;
                    } else
                    {
                        e.Cell = (ITable.ICell) eventTarget;
                    }

                    return e;
                }

                /**
                 * Генерация экземпляра события "ValueChange". Измененеие
                 * значения ячейки
                 *
                 * @param eventSource Источник события.
                 * @return true - изменение состояния произведено, false - Иначе
                 */
                protected static boolean Rise(Abstract_Cell eventSource, Object prevValue, Object newValue)
                {
                    ValueChange e = Create(eventSource, eventSource, prevValue, newValue);

                    return eventSource.Delegate().Rise(e);
                }
            }
        }

        public static class EventLogic extends ATOMEventDelegate
        {
        }

        public IATOMEventDelegate Delegate();

        public IATOMEventLogic EventLogic();
    }
}
