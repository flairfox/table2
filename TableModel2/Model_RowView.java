/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atom.data.TableModel2;

import atom.events2.IATOMEventDelegate;
import atom.events2.IATOMEventLogic;
import java.util.LinkedList;

/**
 *
 * @author Vadim
 */
public class Model_RowView extends Abstract_Row<Model_TableView, Model_RowView, Model_ColumnView, Model_CellView> implements ITableView.IRowView<Model_TableView, Model_RowView, Model_ColumnView, Model_CellView>
{
    private ITableView _OwnerTable;
    private Model_TableView _NestedTable;
    private ITable.IRow _DataRow;

    @Override
    protected Model_CellView createCellInstance(Model_RowView ownerRow)
    {
        Model_CellView newCell = new Model_CellView();
        newCell.setOwnerRow(ownerRow);
        return newCell;
    }

    @Override
    public void setDataRow(ITable.IRow dataRow)
    {
        _DataRow = dataRow;
    }

    @Override
    public ITable.IRow getDataRow()
    {
        return _DataRow;
    }
}
