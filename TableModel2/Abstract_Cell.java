package atom.data.TableModel2;

import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogic;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.IATOMEvent;
import atom.events2.IATOMEventDelegate;

/**
 * Класс, представляющий модель ячейки таблицы
 *
 * @author Добровольский
 */
public abstract class Abstract_Cell<TTable extends ITable<TTable, TRow, TColumn, TCell>,
                        TRow extends ITable.IRow<TTable, TRow, TColumn, TCell>, 
                        TColumn extends ITable.IColumn<TTable, TRow, TColumn, TCell>, 
                        TCell extends ITable.ICell<TTable, TRow, TColumn, TCell>> 
                        implements ITable.ICell<TTable, TRow, TColumn, TCell>, IATOMEventDelegateOwner, IATOMEventLogicOwner
{
    
    // Test Commit

    private TRow _OwnerRow;
    private Object _Value;
    
    @Override
    public void setOwnerRow(TRow ownerRow)
    {
        _OwnerRow = ownerRow;
    }

    @Override
    public void setValue(Object value)
    {
        _Value = value;
        //Events.ValueChange.Rise(this, getValue(), value);
    }

    @Override
    public Object getValue()
    {
        return _Value;
    }

    @Override
    public TRow getOwnerRow()
    {
        return _OwnerRow;
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    /**
     * Делегат событий класса
     */
    //protected IATOMEventDelegate _Delegate = new ATOMEventDelegate(); // для классов без собственной логики событий
    protected IATOMEventDelegate _Delegate = new EventLogic(); // Для класса с собственной логикой событий

    @Override
    public IATOMEventDelegate Delegate()
    {
        return _OwnerRow.Delegate();              //Альтернатива 1. Класс ведет собственный учет слушателей для генерируемых им событий
        //                и/или имеет логику обработки собственных генерируемых событий
        //return _OwnerManager.Delegate();  //Альтернатива 2. Класс полностью перенаправляет учет, генерацию событий и обработку логики в управляющий класс
        // Используется, когда класс может генерировать события, но в программе создается большое число
        // экземпляров классов  при редких индивидуальных подписчиках. Позволяет иключить необходимость
        // подписываться управляющему классу к каждому подчиненному экземпляру подчиненного класса.
        // Пример 1. Класс "Ячейка в таблице" может перенаправлять учет и обработку событий в
        // класс "Строка таблицы", которая в свою очередь может перенаправлять управление в
        // класс "Таблица".
        // Пример 2. Класс "Атрибут сущности данных" может перенаправлять учет и обработку событий
        // в класс "Сущность данных".

    }

    /*Логика обработки событий атрибута*/
    @Override
    @SuppressWarnings("unchecked")
    public IATOMEventLogic EventLogic()
    {
        return _Delegate;
    }

    @Override
    @SuppressWarnings("unchecked")
    public int getRowIndex()
    {
        return _OwnerRow.getOwnerTable().getRowIndex(_OwnerRow);
    }

    @SuppressWarnings("unchecked")    
    @Override
    public int getColumnIndex()
    {
        return _OwnerRow.getCellIndex((TCell) this);
    }

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ITable.ICell.EventLogic        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            if (Events.ValueChange.class.isAssignableFrom(eventClass))
            {
                Events.ValueChange event = (Events.ValueChange) e;
                isAllowEvent = OnBefore_ValueChange(event);
            }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.ValueChange.class.isAssignableFrom(eventClass))
            {
                Events.ValueChange event = (Events.ValueChange) e;
                isDone = OnDo_ValueChange(event);
            }
            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
        private boolean OnBefore_ValueChange(Events.ValueChange event)
        {
            if (_Value == null)
            {
                return (event.newValue == null) ? false : true;
            }

            return (_Value.equals(event.newValue)) ? false : true;
        }

        private boolean OnDo_ValueChange(Events.ValueChange event)
        {
            _Value = event.newValue;

            return true;
        }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
