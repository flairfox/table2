package atom.data.TableModel2;

import java.lang.reflect.Type;
import java.util.LinkedList;

/**
 * Интерфейс обертки над моделью данных. Позволяет настроить параметры
 * отображения данных в виджете
 *
 * @author Добровольский
 */
public interface ITableView<TTableView extends ITableView<TTableView, TRowView, TColumnView, TCellView>,
                            TRowView extends ITableView.IRowView<TTableView, TRowView, TColumnView, TCellView>, 
                            TColumnView extends ITableView.IColumnView<TTableView, TRowView, TColumnView, TCellView>,
                            TCellView extends ITableView.ICellView<TTableView, TRowView, TColumnView, TCellView>> 
                            extends ITable<TTableView, TRowView, TColumnView, TCellView>
{
    public void insertRow(int index, ITable.IRow row);
    
    public void RegisterNestedTableViewStructure(Type userObjectType, ITableViewStructure tableViewSetup);
    public ITableView CreateTableViewStructure(ITable.IRow expandableRow);
    
    @Override
    public TRowView createDefaultRowInstance();
    
    @Override
    public TColumnView createDefaultColumnInstance();
    
    @Override
    public TCellView createDefaultCellInstance(TRowView ownerRow);

    @Override
    public TRowView getRow(int index);

    @Override
    public LinkedList<TRowView> getRows();

    public void insertColumn(int index, String attributeName, String columnCaption);

    public void insertCalculatedColumn(int index, ICalculator calculator, String columnCaption);

    @Override
    public TColumnView getColumn(int index);

    @Override
    public LinkedList<TColumnView> getColumns();

    public void setDataModel(ITable dataModel);
    
    public interface IRowView<TTableView extends ITableView<TTableView, TRowView, TColumnView, TCellView>,
                              TRowView extends ITableView.IRowView<TTableView, TRowView, TColumnView, TCellView>,
                              TColumnView extends ITableView.IColumnView<TTableView, TRowView, TColumnView, TCellView>,
                              TCellView extends ITableView.ICellView<TTableView, TRowView, TColumnView, TCellView>>
                              extends IRow<TTableView, TRowView, TColumnView, TCellView>
    {
        public void setDataRow(ITable.IRow dataRow);
        public ITable.IRow getDataRow();
    }
    
    public interface IColumnView<TTableView extends ITable<TTableView, TRowView, TColumnView, TCellView>,
                                 TRowView extends ITable.IRow<TTableView, TRowView, TColumnView, TCellView>,
                                 TColumnView extends ITable.IColumn<TTableView, TRowView, TColumnView, TCellView>,
                                 TCellView extends ITable.ICell<TTableView, TRowView, TColumnView, TCellView>>
                                 extends ITable.IColumn<TTableView, TRowView, TColumnView, TCellView>
    {
        public String getCaption();
    }
    
    public interface ICellView<TTableView extends ITable<TTableView, TRowView, TColumnView, TCellView>,
                               TRowView extends ITable.IRow<TTableView, TRowView, TColumnView, TCellView>,
                               TColumnView extends ITable.IColumn<TTableView, TRowView, TColumnView, TCellView>,
                               TCellView extends ITable.ICell<TTableView, TRowView, TColumnView, TCellView>>
                               extends ITable.ICell<TTableView, TRowView, TColumnView, TCellView>
    {
        
    }
}
