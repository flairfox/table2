package atom.data.TableModel2;

/**
 *
 * @author Добровольский
 */
public interface ICalculator<TTable extends ITable<TTable, TRow, TColumn, TCell>,
                             TRow extends ITable.IRow<TTable, TRow, TColumn, TCell>,
                             TColumn extends ITable.IColumn<TTable, TRow, TColumn, TCell>,
                             TCell extends ITable.ICell<TTable, TRow, TColumn, TCell>>
{
    public Object calculateValue(TRow row);
}
