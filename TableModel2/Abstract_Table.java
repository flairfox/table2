package atom.data.TableModel2;

import atom.data.DataTypeInfo;
import java.util.LinkedList;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogic;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.ATOMEventDelegate;
import atom.events2.IATOMEvent;
import atom.events2.IATOMEventDelegate;
import atom.names.ATOMName;
import atom.names.IATOMName;
import java.util.Random;

/**
 * Класс, представляющий модель таблицы
 *
 * @author Добровольский
 */
public abstract class Abstract_Table<TTable extends ITable<TTable, TRow, TColumn, TCell>,
                                     TRow extends ITable.IRow<TTable, TRow, TColumn, TCell>, 
                                     TColumn extends ITable.IColumn<TTable, TRow, TColumn, TCell>, 
                                     TCell extends ITable.ICell<TTable, TRow, TColumn, TCell>> 
                                     implements ITable<TTable, TRow, TColumn, TCell>, IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    /**
     * Коллекция рядов таблицы
     */
    protected LinkedList<TRow> _Rows;

    /**
     * Коллекция столбцов таблицы
     */
    protected LinkedList<TColumn> _Columns;

    private Random _RandomInt;

    public Abstract_Table()
    {
        _Rows = new LinkedList<TRow>();
        _Columns = new LinkedList<TColumn>();
    }
    
    public void initTable(int rows)
    {
        String[] attributes =
        {
            "secondName", "firstName", "patronymic", "gender", "dateOfBirth", "age", "profession"
        };

        for (String arrtibute : attributes)
        {
            TColumn newColumn = createDefaultColumnInstance();
            newColumn.setName("Name " + arrtibute);
            newColumn.setRole("Role " + arrtibute);
            newColumn.setAttributeName(arrtibute);
            newColumn.setType(DataTypeInfo.ELogicTypes.String);

            addColumn(newColumn);
        }
        
        for (int i = 0; i < rows; i++)
        {
            addRow();
        }
    }

    public void initTable()
    {
        _RandomInt = new Random();

        int min = 5,
                max = 15;
        int rowCount = min + (int) (_RandomInt.nextDouble() * ((max - min) + 1));
        int columnCount = min + (int) (_RandomInt.nextDouble() * ((max - min) + 1));

        for (int i = 0; i < columnCount; i++)
        {
            TColumn newColumn = createDefaultColumnInstance();
            newColumn.setName("Name " + i);
            newColumn.setRole("Role " + i);
            newColumn.setAttributeName("Attribute " + i);
            newColumn.setType(DataTypeInfo.ELogicTypes.String);
            
            addColumn(newColumn);
        }

        for (int i = 0; i < rowCount; i++)
        {
            addRow();
        }
    }
    
    @Override
    public int getColumnIndex(TColumn column)
    {
        return _Columns.indexOf(column);
    }
    
    @Override
    public abstract TRow createDefaultRowInstance();
    
    @Override
    public abstract TColumn createDefaultColumnInstance();
    
    @Override
    public abstract TCell createDefaultCellInstance(TRow ownerRow);
    
    @Override
    public TRow addRow()
    {
        return addRow(_Rows.size());
    }

    @Override
    public TRow addRow(int index)
    {
        TRow newRow = createDefaultRowInstance();
        Events.AddRow.Rise(this, index, newRow);

        return newRow;
    }

    @Override
    public void addRow(TRow newRow)
    {
        Events.AddRow.Rise(this, this.getRowCount(), newRow);
    }

    @Override
    public void addRow(int index, TRow newRow)
    {
        Events.AddRow.Rise(this, index, newRow);
    }

    @Override
    public TRow getRow(int index)
    {
        return _Rows.get(index);
    }

    @Override
    public LinkedList<TRow> getRows()
    {
        return _Rows;
    }

    @Override
    public int getRowCount()
    {
        return _Rows.size();
    }

    @Override
    public void removeRow(int index)
    {
        _Rows.remove(index);
    }

    @Override
    public void addColumn(String name, String role, DataTypeInfo.ELogicTypes dataType)
    {
        TColumn newColumn = createDefaultColumnInstance();
        newColumn.setName(name);
        newColumn.setRole(role);
        newColumn.setType(dataType);
        
        Events.AddColumn.Rise(this, newColumn);
    }

    @Override
    public void addColumn(String name, DataTypeInfo.ELogicTypes dataType)
    {
        addColumn(name, "", dataType);
    }

    @Override
    public void addColumn(TColumn newColumn)
    {
        Events.AddColumn.Rise(this, newColumn);
    }

    @Override
    public TColumn getColumn(int index)
    {
        return _Columns.get(index);
    }

    @Override
    public TColumn getColumn(String name, String role)
    {
        for (TColumn column : _Columns)
        {
            if ((column.getName().equals(name)) && (column.getRole().equals(role)))
            {
                return column;
            }
        }
        return null;
    }

    @Override
    public LinkedList<TColumn> getColumns()
    {
        return _Columns;
    }

    @Override
    public int getColumnCount()
    {
        return _Columns.size();
    }

    @Override
    public void removeColumn(int index)
    {
        _Columns.remove(index);
    }

    @Override
    public void removeColumn(String name, String role)
    {
        _Columns.remove(getColumn(name, role));
    }

    @Override
    public TCell getCell(int rowIndex, int columnIndex)
    {
        return _Rows.get(rowIndex).getCell(columnIndex);
    }

    @Override
    public int getRowIndex(IRow row)
    {
        return _Rows.indexOf(row);
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    /**
     * Делегат событий класса
     */
    //protected IATOMEventDelegate _Delegate = new ATOMEventDelegate(); // для классов без собственной логики событий
    protected IATOMEventDelegate _Delegate = new EventLogic(); // Для класса с собственной логикой событий

    @Override
    public IATOMEventDelegate Delegate()
    {
        return _Delegate;              //Альтернатива 1. Класс ведет собственный учет слушателей для генерируемых им событий
        //                и/или имеет логику обработки собственных генерируемых событий
        //return _OwnerManager.Delegate();  //Альтернатива 2. Класс полностью перенаправляет учет, генерацию событий и обработку логики в управляющий класс
        // Используется, когда класс может генерировать события, но в программе создается большое число
        // экземпляров классов  при редких индивидуальных подписчиках. Позволяет иключить необходимость
        // подписываться управляющему классу к каждому подчиненному экземпляру подчиненного класса.
        // Пример 1. Класс "Ячейка в таблице" может перенаправлять учет и обработку событий в
        // класс "Строка таблицы", которая в свою очередь может перенаправлять управление в
        // класс "Таблица".
        // Пример 2. Класс "Атрибут сущности данных" может перенаправлять учет и обработку событий
        // в класс "Сущность данных".

    }

    /*Логика обработки событий атрибута*/
    @Override
    public IATOMEventLogic EventLogic()
    {
        return _Delegate;
    }

    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ITable.Events
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super(eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 1
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Добавление нового столбца в таблицу" #####">
        /**
         * Класс экземпляра события "Добавление нового столбца в таблицу"
         */
        public static class AddColumn extends Events
        {

            /**
             * Тип события "Добавление нового столбца в таблицу"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.AddColumn");

            //Параметры события
            public IColumn newColumn;

            //int parametr2;
            public AddColumn(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public AddColumn(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static AddColumn Create(IATOMEventDelegateOwner eventSource, Object eventTarget, IColumn newColumn)
            {
                AddColumn e = new AddColumn(eventSource, eventTarget);
                e.newColumn = newColumn;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Добавление нового столбца
             * в таблицу* @param eventSource Источник события.
             *
             * @param eventSource
             * @param newColumn
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Abstract_Table eventSource, IColumn newColumn)
            {
                AddColumn e = Create(eventSource, eventSource, newColumn);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Добавление нового столбца в таблицу""
//##################################################################

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 2
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Добавление строки в таблицу" #####">
        /**
         * Класс экземпляра события "Добавление строки в таблицу"
         */
        public static class AddRow extends Events
        {

            /**
             * Тип события "Добавление строки в таблицу"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.AddRow");

            //Параметры события
            public int index;
            public IRow newRow;

            //int parametr1;
            //int parametr2;
            public AddRow(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public AddRow(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static AddRow Create(IATOMEventDelegateOwner eventSource, Object eventTarget, int index, IRow newRow)
            {
                AddRow e = new AddRow(eventSource, eventTarget);
                e.index = index;
                e.newRow = newRow;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Добавление строки в
             * таблицу* @param eventSource Источник события.
             *
             * @param eventSource
             * @param index
             * @param newRow
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Abstract_Table eventSource, int index, IRow newRow)
            {
                AddRow e = Create(eventSource, eventSource, index, newRow);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Добавление строки в таблицу""
//##################################################################

    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMEventDelegate        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            if (ITable.IRow.Events.class.isAssignableFrom(eventClass))
            {
                ITable.IRow.Events event = (ITable.IRow.Events) e;
                isAllowEvent = event.Row.EventLogic().OnBeforeEvent(eventSource, e);
            }
            //2. Событие Events.Event2
            if (Abstract_Table.Events.AddColumn.class.isAssignableFrom(eventClass))
            {
                Abstract_Table.Events.AddColumn event = (Abstract_Table.Events.AddColumn) e;
                isAllowEvent = OnBefore_AddColumn(event);
            }

            if (Abstract_Table.Events.AddRow.class.isAssignableFrom(eventClass))
            {
                Abstract_Table.Events.AddRow event = (Abstract_Table.Events.AddRow) e;
                isAllowEvent = OnBefore_AddRow(event);
            }

            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (ITable.IRow.Events.class.isAssignableFrom(eventClass))
            {
                ITable.IRow.Events event = (ITable.IRow.Events) e;
                isDone = event.Row.EventLogic().OnDoEvent(eventSource, e);
            }

            if (Abstract_Table.Events.AddColumn.class.isAssignableFrom(eventClass))
            {
                Abstract_Table.Events.AddColumn event = (Abstract_Table.Events.AddColumn) e;
                isDone = OnDo_AddColumn(event);
            }

            if (Abstract_Table.Events.AddRow.class.isAssignableFrom(eventClass))
            {
                Abstract_Table.Events.AddRow event = (Abstract_Table.Events.AddRow) e;
                isDone = OnDo_AddRow(event);
            }

            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
        private boolean OnBefore_AddColumn(Events.AddColumn event)
        {
            return !_Columns.contains(event.newColumn);
        }

        @SuppressWarnings("unchecked") 
        private boolean OnDo_AddColumn(Events.AddColumn event)
        {
            _Columns.add((TColumn) event.newColumn);
            for (TRow row : _Rows)
            {
                TCell newCell = createDefaultCellInstance(row);
                row.addCell(newCell);
            }
            return true;
        }

        private boolean OnBefore_AddRow(Events.AddRow event)
        {
            return !_Rows.contains(event.newRow);
        }

        @SuppressWarnings("unchecked") 
        private boolean OnDo_AddRow(Events.AddRow event)
        {
            _Rows.add(event.index, (TRow) event.newRow);
            return true;
        }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
