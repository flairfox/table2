package atom.data.TableModel2;

import java.util.LinkedList;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogic;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.ATOMEventDelegate;
import atom.events2.IATOMEvent;
import atom.events2.IATOMEventDelegate;

/**
 * Класс представляющий модель ряда
 *
 * @author Добровольский
 */
public abstract class Abstract_Row<TTable extends ITable<TTable, TRow, TColumn, TCell>,
                       TRow extends ITable.IRow<TTable, TRow, TColumn, TCell>, 
                       TColumn extends ITable.IColumn<TTable, TRow, TColumn, TCell>, 
                       TCell extends ITable.ICell<TTable, TRow, TColumn, TCell>> 
                       implements ITable.IRow<TTable, TRow, TColumn, TCell>, IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    protected TTable _OwnerTable;
    protected TTable _NestedTable;
    
    protected LinkedList<TCell> _Cells;
    protected String _type;
    protected Object _UserData;

    @SuppressWarnings("unchecked") 
    public Abstract_Row()
    {
        _Cells = new LinkedList<TCell>();
        this._type = "Person";
    }
    
    @SuppressWarnings("unchecked")    
    @Override
    public void setOwnerTable(TTable ownerTable)
    {
        _OwnerTable = ownerTable;

        for (TColumn column : (LinkedList<TColumn>) _OwnerTable.getColumns())
        {
            TCell newCell = createCellInstance((TRow) this);
            addCell(newCell);
        }
    }
    
    @Override
    public TTable getNestedTable()
    {
        return _NestedTable;
    }
    
    @Override
    public void setNestedTable(TTable nestedTable)
    {
        _NestedTable = nestedTable;
    }
    
    protected abstract TCell createCellInstance(TRow ownerRow);

    @Override
    public int getCellCount()
    {
        return _Cells.size();
    }

    @Override
    public void addCell(int index, TCell newCell)
    {
        _Cells.add(index, newCell);
    }

    @Override
    public void addCell(TCell newCell)
    {
        _Cells.add(newCell);
    }

    @SuppressWarnings("unchecked")    
    @Override
    public TCell addCell()
    {
        TCell newCell = createCellInstance((TRow) this);
        _Cells.add(newCell);
        return newCell;
    }

    @Override
    public TCell getCell(int index)
    {
        return _Cells.get(index);
    }
    
    @Override
    public TCell getCell(String attributeName)
    {
        for(TColumn column : _OwnerTable.getColumns())
        {
            if(column.getAttributeName().equals(attributeName))
            {
                return getCell(_OwnerTable.getColumnIndex(column));
            }
        }
        
        return null;
    }

    @Override
    public void setUserObject(Object userData)
    {
        _UserData = userData;
    }

    @Override
    public Object getUserObject()
    {
        return _UserData;
    }

    @Override
    public TTable getOwnerTable()
    {
        return _OwnerTable;
    }

    @Override
    public void setType(String type)
    {
        _type = type;
    }

    @Override
    public String getType()
    {
        return _type;
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    /**
     * Делегат событий класса
     */
    //protected IATOMEventDelegate _Delegate = new ATOMEventDelegate(); // для классов без собственной логики событий
    protected IATOMEventDelegate _Delegate = new EventLogic(); // Для класса с собственной логикой событий

    @Override
    public IATOMEventDelegate Delegate()
    {
        return _OwnerTable.Delegate();              //Альтернатива 1. Класс ведет собственный учет слушателей для генерируемых им событий
        //                и/или имеет логику обработки собственных генерируемых событий
        //return _OwnerManager.Delegate();  //Альтернатива 2. Класс полностью перенаправляет учет, генерацию событий и обработку логики в управляющий класс
        // Используется, когда класс может генерировать события, но в программе создается большое число
        // экземпляров классов  при редких индивидуальных подписчиках. Позволяет иключить необходимость
        // подписываться управляющему классу к каждому подчиненному экземпляру подчиненного класса.
        // Пример 1. Класс "Ячейка в таблице" может перенаправлять учет и обработку событий в
        // класс "Строка таблицы", которая в свою очередь может перенаправлять управление в
        // класс "Таблица".
        // Пример 2. Класс "Атрибут сущности данных" может перенаправлять учет и обработку событий
        // в класс "Сущность данных".

    }

    /*Логика обработки событий атрибута*/
    @Override
    public IATOMEventLogic EventLogic()
    {
        return _Delegate;
    }

    @Override
    public int getCellIndex(TCell cell)
    {
        return _Cells.indexOf(cell);
    }

    @Override
    public LinkedList<TCell> getCells()
    {
        return _Cells;
    }

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMEventDelegate        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            if (ITable.ICell.Events.class.isAssignableFrom(eventClass))
            {
                ITable.ICell.Events event = (ITable.ICell.Events) e;
                isAllowEvent = event.Cell.EventLogic().OnBeforeEvent(eventSource, e);
            }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (ITable.ICell.Events.class.isAssignableFrom(eventClass))
            {
                ITable.ICell.Events event = (ITable.ICell.Events) e;
                isDone = event.Cell.EventLogic().OnDoEvent(eventSource, e);
            }
            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
