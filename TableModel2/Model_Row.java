/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atom.data.TableModel2;

/**
 *
 * @author Vadim
 */
public class Model_Row extends Abstract_Row<Model_Table, Model_Row, Model_Column, Model_Cell> implements ITable.IRow<Model_Table, Model_Row, Model_Column, Model_Cell>
{

    @Override
    protected Model_Cell createCellInstance(Model_Row ownerRow)
    {
        Model_Cell newCell = new Model_Cell();
        newCell.setOwnerRow(ownerRow);
        return newCell;
    }
    
}
