package atom.data.TableModel2;

/**
 *
 * @author Добровольский
 */
public interface IFilterLogic<T>
{
    public boolean isFiltered(T item);
}
