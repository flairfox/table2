package atom.data.TableModel2;

/**
 *
 * @author Добровольский
 */
public interface ITableViewStructure
{
    public ITableView createTableViewSetup(ITable.IRow expandlableRow);
}
