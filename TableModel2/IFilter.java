package atom.data.TableModel2;

/**
 *
 * @author Добровольский
 */
public interface IFilter<T>
{
    public void addFilterLogic(IFilterLogic filterLogic);
    public void removeFilterLogic(IFilterLogic filterLogic);
    
    public boolean isFiltered(T row);
}
