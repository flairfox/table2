package atom.data.TableModel2;

/**
 *
 * @author Добровольский
 */
public interface ISorter<TCollection>
{
    public void addSortLogic(ISortLogic sortLogic);
    public void removeSortLogic(ISortLogic sortLogic);

    public TCollection sortAscending(TCollection collection);
    public TCollection sortDescending(TCollection collection);
}
