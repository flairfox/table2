package atom.data.TableModel2;

import java.util.Comparator;

/**
 *
 * @author Добровольский
 */
public interface ISortLogic<T> extends Comparator<T>
{
    @Override
    public int compare(T item1, T item2);
}
