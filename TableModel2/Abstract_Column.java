package atom.data.TableModel2;

import atom.data.DataTypeInfo;
import java.util.LinkedList;

/**
 * Класс, представляющий модель столбца таблицы
 *
 * @author Добровольский
 */
public abstract class Abstract_Column<TTable extends ITable<TTable, TRow, TColumn, TCell>,
                         TRow extends ITable.IRow<TTable, TRow, TColumn, TCell>, 
                         TColumn extends ITable.IColumn<TTable, TRow, TColumn, TCell>, 
                         TCell extends ITable.ICell<TTable, TRow, TColumn, TCell>> 
                         implements ITable.IColumn<TTable, TRow, TColumn, TCell>
{

    private TTable _OwnerTable;
    
    private Object _UserObject;
    
    private String _AttributeName;
    private String _Name;
    private String _Role;
    private DataTypeInfo.ELogicTypes _Type;

    public Abstract_Column(TTable ownerTable, String name, String role, DataTypeInfo.ELogicTypes type)
    {
        _OwnerTable = ownerTable;
        _Name = name;
        _Role = role;
        _Type = type;
    }
    
    @Override
    public void setOwnerTable(TTable ownerTable)
    {
        _OwnerTable = ownerTable;
        
        for (TRow row : (LinkedList<TRow>) _OwnerTable.getRows())
        {
            TCell newCell = createCellInstance(row);
            row.addCell(newCell);
        }
    }
    
    protected abstract TCell createCellInstance(TRow ownerRow);
    
    @Override 
    public String getAttributeName()
    {
        return _AttributeName;
    }
    
    @Override
    public void setAttributeName(String attributeName)
    {
        _AttributeName = attributeName;
    }

    @Override
    public void setName(String name)
    {
        _Name = name;
    }

    @Override
    public String getName()
    {
        return _Name;
    }

    @Override
    public void setRole(String role)
    {
        _Role = role;
    }

    @Override
    public String getRole()
    {
        return _Role;
    }

    @Override
    public void setType(DataTypeInfo.ELogicTypes type)
    {
        _Type = type;
    }

    @Override
    public DataTypeInfo.ELogicTypes getType()
    {
        return _Type;
    }

    @Override
    public TTable getOwnerTable()
    {
        return _OwnerTable;
    }

    @Override
    public TCell getCell(int rowIndex)
    {
        int columnIndex = _OwnerTable.getColumns().indexOf(this);

        TCell cell = _OwnerTable.getCell(rowIndex, columnIndex);

        return cell;
    }
    
    @Override
    public void setUserObject(Object userObject)
    {
        _UserObject = userObject;
    }
    
    @Override
    public Object getUserObject()
    {
        return _UserObject;
    }
}
