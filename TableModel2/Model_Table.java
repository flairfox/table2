package atom.data.TableModel2;

import atom.data.DataTypeInfo;

/**
 *
 * @author Добровольский
 */
public class Model_Table extends Abstract_Table<Model_Table, Model_Row, Model_Column, Model_Cell> implements ITable<Model_Table, Model_Row, Model_Column, Model_Cell>
{

    @Override
    public Model_Row createDefaultRowInstance()
    {
        Model_Row newRow = new Model_Row();
        newRow.setOwnerTable(this);
        return newRow;
    }

    @Override
    public Model_Column createDefaultColumnInstance()
    {
        return new Model_Column(this, "", "", DataTypeInfo.ELogicTypes.String);
    }

    @Override
    public Model_Cell createDefaultCellInstance(Model_Row ownerRow)
    {
        Model_Cell newCell = new Model_Cell();
        newCell.setOwnerRow(ownerRow);
        return newCell;
    }
}
